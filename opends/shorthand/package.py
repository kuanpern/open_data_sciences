import os
import sys
from setuptools import find_packages
from pkgutil import iter_modules
import types
import uuid
import anytree
from anytree import Node, RenderTree
import importlib
import logging
logger = logging.getLogger(__name__)

def find_modules(path):
	modules = set()
	for pkg in find_packages(path):
		modules.add(pkg)
		pkgpath = path + '/' + pkg.replace('.', '/')
		if sys.version_info.major == 2 or (sys.version_info.major == 3 and sys.version_info.minor < 6):
			for _, name, ispkg in iter_modules([pkgpath]):
				if not ispkg:
					modules.add(pkg + '.' + name)
		else:
			for info in iter_modules([pkgpath]):
				if not info.ispkg:
					modules.add(pkg + '.' + info.name)
	return modules
# end def

def list_module_function_names(mod):
	path = os.path.dirname(mod.__file__)
	sys.path.append(path)
	exec('import {mod}'.format(mod=mod.__name__), globals())

	output = sorted(find_modules(path))
	output = [mod.__name__+'.'+_ for _ in output]
	swap = []
	for name in output:
		_dict = locals()
		try:
			exec('temp = dir({name})'.format(name=name), globals(), _dict)
			temp = _dict['temp']
			temp = [_ for _ in temp if not(_.startswith('__'))]
			swap.extend([name+'.'+_ for _ in temp])
		except AttributeError:
			logger.warning('Cannot import "{name}"'.format(name=name))
			continue
		# end try
	# end for
	output = swap

	sels = []
	for name in output:
		_dict = locals()
		exec('det_p = isinstance({name}, types.FunctionType)'       .format(name=name), globals(), _dict)
		exec('det_q = isinstance({name}, types.BuiltinFunctionType)'.format(name=name), globals(), _dict)
		if _dict['det_p'] or _dict['det_q']:
			sels.append(name)
		# end if
	# end for
	return sels
# end def
