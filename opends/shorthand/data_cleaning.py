import io
import datetime
import dateutil.parser
import random
import numpy as np
import pandas as pd
import collections
import scipy.stats
import opends.shorthand
import opends.stats
import openpyxl
import logging
logger = logging.getLogger()


def guess_datetime_type(s, max_failure=100, cutoff_ratio=0.5, min_date='1900-01-01', max_date='2100-01-01'):
	"""Guess the whether the input series is of datetime type

	Args:
		:s: Input series
		:max_failure: Maximum number of failed parsing action to return False
		:cutoff_ratio: Mininum ratio to reach to return True
		:min_date: Parsed date must be greater than this to be considered valid
		:max_date: Parsed date must be lower than this to be considered valid

	Returns:
		:Bool: Whether the series is (guessed to be) of datetime type
	"""

	min_date = dateutil.parser.parse(min_date)
	max_date = dateutil.parser.parse(max_date)
	
	n = 0 # good count
	b = 0 # bad count
	m = 0 # all count
	for v in s:
		if b >= max_failure:
			return False
		# end if

		# skip N.A.
		if v != v:
			continue
		# end if
		m += 1
		
		# skip integer
		try:
			int(v)
			b += 1
			continue
		except ValueError:
			pass
		# end try
		
		try:
			# parse
			t = dateutil.parser.parse(str(v))
			# valid value
			if min_date <= t <= max_date:
				n += 1
			# end if
		except (ValueError, OverflowError) as e:
			b += 1
			continue
		# end try

	# end for

	# check ratio
	if n / m >= cutoff_ratio:
		return True
	else:
		return False
	# end if
# end def

def sample_csv_lines(infile, ntop=10, nlines=1000, p_choose_line=0.1, max_iter=1000, skip_nlines=1):
	"""Sample random lines from CSV file

	Args:
		:infile: Input csv file path
		:ntop: Number of top lines to be included
		:nlines: Number of lines to sample
		:p_choose_line: Probability to sample a line
		:max_iter: Maximum iteration number (for emergency stop)
		:skip_nlines: Number of top lines to be excluded (usually header) from 2nd iteration onwards


	Returns:
		:Tuple: swapped objects
	"""

	def epoch(lines, counter, nlines):
		with open(infile, 'r') as fin:
			for i in range(skip_nlines):
				next(fin)
			# end for
			for line in fin:
				if random.random() < p_choose_line:
					line = next(fin)
					lines += line
					counter += 1
					if counter >= nlines:
						break
					# end if
				# end if
			# end for
		# end with
		return lines, counter
	# end def

	counter = 0
	lines = ''

	# top 10 lines first (incl header)
	with open(infile, 'r') as fin:
		for i in range(ntop):
			line = next(fin)
			lines += line
			counter += 1
		# end for
	# end with
	
	# read randomly
	_iter = 0
	while True:
		_iter += 1
		if _iter >= max_iter:
			break # emergency break
		lines, counter = epoch(lines, counter, nlines)
		if counter > nlines:
			break
		#end if
	# end while

	# read into dataframe
	sio = io.StringIO(lines)
	df = pd.read_csv(sio)
	return df.drop_duplicates()
# end def


# guessing for datetime columns
def guess_datetime_cols(df, max_failure=100, cutoff_ratio=0.5, min_date='1900-01-01', max_date='2100-01-01'):
	"""Guess the datetime columns in a dataframe

	Args:
		:df: pandas dataframe
		:max_failure: Maximum number of failed parsing action to return False
		:cutoff_ratio: Mininum ratio to reach to return True
		:min_date: Parsed date must be greater than this to be considered valid
		:max_date: Parsed date must be lower than this to be considered valid

	Returns:
		:List: Guessed datetime type column names
	"""

	datetime_cols = []
	for col in df.columns:
		if guess_datetime_type(df[col]) is True:
			datetime_cols.append(col)
		# end if
	# end for
	return datetime_cols
# end def

def guess_unique_cols(df, unique_cutoff=0.9):
	"""Guess the unique columns in a dataframe

	Args:
		:df: pandas dataframe
		:unique_cutoff: Cut-off ratio for uniqueness

	Returns:
		:List: Guessed unique-valued column names
	"""

	temp = []
	for col in df.columns:
		_df = df[col].dropna()
		r = _df.nunique() / len(_df)
		temp.append([col, r])
	# end for
	
	df_uniqueness = pd.DataFrame(temp, columns=['name', 'uniqueness'])
	labelling_columns = list(df_uniqueness[df_uniqueness['uniqueness'] > unique_cutoff]['name'].values)
	return labelling_columns, df_uniqueness
# end def


def guess_categorical_cols(df, category_cutoff=30):
	"""Guess the categorical columns in a dataframe

	Args:
		:df: pandas dataframe
		:category_cutoff: Maximum of number of category

	Returns:
		:Tuple: swapped objects
	"""

	category_nums = {col: df[col].nunique() for col in df.columns}
	categoricals = [key for key, val in category_nums.items() if val <= category_cutoff]
	return categoricals
# end def


def guess_dataframe_column_types(DF, nrows=1000, random_state=None, category_cutoff=30, unique_cutoff=0.9):
	"""Guess the datatype of columns in a dataframe using empirical rules

	Args:
		:DF: pandas dataframe
		:nrows: number of rows to sample for datatype guessing
		:category_cutoff: Maximum of number of category for categorical column guesswork
		:unique_cutoff: Cut-off ratio for unique-valued guesswork

	Returns:
		:Tuple: swapped objects
	"""

	column_types = collections.defaultdict(lambda: [])

	df = pd.DataFrame()
	for col in DF.columns:
		_df = DF[col].dropna()
		if len(_df) < nrows:
			column_types['unsure'].append(col)
			continue
		# end if
		df[col] = _df.sample(n=nrows, replace=False).values
	# end for

	# guess for datetime
	logger.info('guessing for datetime columns')
	column_types['datetime'] = guess_datetime_cols(df)
	# exclude datetime columns
	df = df.drop(columns=column_types['datetime'])

	# Categorical columns
	logger.info('guessing for categorical columns')
	column_types['categorical'] = guess_categorical_cols(df, category_cutoff=category_cutoff)
	df = df.drop(columns=column_types['categorical'])

	# guess unique columns
	logger.info('guessing for uniquely valued columns')
	cols, _ = guess_unique_cols(df, unique_cutoff=unique_cutoff)
	column_types['unique'] = cols
	df = df.drop(columns=column_types['unique'])

	# other = numeric and free text
	logger.info('guessing for numeric and free text columns')
	rec = {}
	for col in df.columns:
		rec[col] = pd.api.types.infer_dtype(df[col].dropna())
	# end for
	swap = collections.defaultdict(lambda: [])
	for key, val in rec.items():
		swap[val].append(key)
	# end for
	swap = dict(swap)

	column_types['numeric'] = swap.pop('floating', []) + swap.pop('integer', [])
	column_types['text'] = swap.pop('string', [])
	column_types.update(swap)
	column_types = dict(column_types)

	return column_types
# end def

