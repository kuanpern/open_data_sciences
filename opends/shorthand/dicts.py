import copy
import collections
from collections import OrderedDict

import collections
class InfiniteDict(collections.defaultdict):
    def __init__(self, D=None):
        '''Intialize an infinitely deep defaultdict'''
        collections.defaultdict.__init__(self, self.__class__)

        if D is not None:
            self.update(copy.deepcopy(D))
        # end ifx
    # end def


    def _nested_update(self, d, u):
        for k, v in u.items():
            if isinstance(v, collections.abc.Mapping):
                d[k] = self._nested_update(d.get(k, {}), v)
            else:
                d[k] = v
            # end if
        return d
    # end def
        
    def update(self, u):
        d = self._nested_update(self, u)
        for key in list(set(self.keys()) - set(d.keys())):
            self.pop(key);
        # end for
        for key, val in d.items():
            if isinstance(val, collections.abc.Mapping):
                self[key] = self.__createInfiniteDict(val)
            else:
                self[key] = val
            # end if
        # end for        
    # end def
    

    def to_dict(self):
        '''Convert to vanilla dictionary type

        Returns:
            :Dict: vanilla dictionary representation of this object
        '''
        return self.__todict(self)
    # end def

    def locate(self, path):
        '''Locate an item using a path, which is a tuple of the multi-level

        Args:
            :path (tuple): a tuple of multi-level keys

        Returns:
            :Object: value or object in the InfiniteDict with the multi-level key specified by path
        '''
        assert isinstance(path, tuple), 'path must of tuple type'

        t = self
        for j in path:
            t = t[j]
        # end for
        return t
    # end def

    def __createInfiniteDict(self, d):
        '''create an InfiniteDict from a dict object

        Args:
            :d: input dictionary

        Returns:
            :InfiniteDict: output InfiniteDict instance
        '''
        assert isinstance(d, dict), 'Input must be of dict type. Current input is %s type' % (str(type(d)),)
        for k, v in d.items():
            if isinstance(v, dict):
                if not(isinstance(v, InfiniteDict)):
                    f = InfiniteDict()
                    for _key, _val in v.items():
                        f[_key] = _val
                    # end for
                    d[k] = f
                # end if 
                d[k] = self.__createInfiniteDict(d[k])
            # end if
        # end for

        D = InfiniteDict()
        for key, val in d.items():
            D[key] = val
        # end for
        return D
    # end def

    def __todict(self, D):
        '''Convert to vanilla dictionary type

        Args:
            :D: input dictionary

        Returns:
            :Dict: vanilla dictionary representation of the input dictionary
        '''
        D = dict(D)
        keys = list(D.keys())
        for k in keys:
            v = D[k]
            if isinstance(v, InfiniteDict):
                D[k] = dict(v)
                D[k] = self.__todict(D[k])
            # end if
        # end for
        return D
    # end def
# end class


class DefaultOrderedDict(OrderedDict):
	# Source: http://stackoverflow.com/a/6190500/562769
	def __init__(self, default_factory=None, *a, **kw):
		if (default_factory is not None and
		   not isinstance(default_factory, Callable)):
			raise TypeError('first argument must be callable')
		OrderedDict.__init__(self, *a, **kw)
		self.default_factory = default_factory
	# end def

	def __getitem__(self, key):
		try:
			return OrderedDict.__getitem__(self, key)
		except KeyError:
			return self.__missing__(key)
	# end def

	def __missing__(self, key):
		if self.default_factory is None:
			raise KeyError(key)
		self[key] = value = self.default_factory()
		return value
	# end def

	def __reduce__(self):
		if self.default_factory is None:
			args = tuple()
		else:
			args = self.default_factory,
		return type(self), args, None, None, self.items()
	# end def

	def copy(self):
		return self.__copy__()
	# end def

	def __copy__(self):
		return type(self)(self.default_factory, self)
	# end def

	def __deepcopy__(self, memo):
		import copy
		return type(self)(self.default_factory,
						  copy.deepcopy(self.items()))
	# end def

	def __repr__(self):
		return 'OrderedDefaultDict(%s, %s)' % (self.default_factory,
											   OrderedDict.__repr__(self))
	# end def
# end class


# taken from https://stackoverflow.com/questions/2437617/how-to-limit-the-size-of-a-dictionary
class FixedSizeDict(OrderedDict):
	def __init__(self, *args, **kwds):
		self.size_limit = kwds.pop("size", None)
		OrderedDict.__init__(self, *args, **kwds)
		self._check_size_limit()
	# end def

	def __setitem__(self, key, value):
		OrderedDict.__setitem__(self, key, value)
		self._check_size_limit()
	# end def

	def _check_size_limit(self):
		if self.size_limit is not None:
			while len(self) > self.size_limit:
				self.popitem(last=False)
			# end while
		# end if
	# end def
# end class

