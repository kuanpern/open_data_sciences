"""
Shorthand Functions
"""

import os
import sys
import jinja2
import time
import errno
import numpy
import scipy
import scipy.stats
import random
import subprocess
import string
import dateutil
import uuid
import requests
import signal
import datetime
import copy
import yaml
import base64
import inspect
import argparse
import urllib
import hashlib
import collections
from collections import OrderedDict, Callable
from functools import wraps
from .dicts import *

import logging
logger = logging.getLogger()

from .data_cleaning import *
from .package import *
from .notebook import *

def next_day(n=1):
	"""Return a datetime object n days from now

	Args:
		:n (Int): number of days from now

	Returns:
		:Datetime object
	"""
	# this
	return (datetime.date.today() + datetime.timedelta(n)).isoformat()
# end def


# IO, formats
def yesterday():
	"""Return datetime object for yesterday
	"""
	# this
	return next_day(-1)
# end def


def today():
	"""Return datetime object for today
	"""
	# this
	return next_day(0)
# end def


def tomorrow():
	"""Return datetime object for tomorrow
	"""
	# this
	return next_day(1)
# end def


def subdivide_data(S, size=50):
	"""Sub-divide the input series into parts of certain size

	Args:
		:S (Series): Input series
		:size (Int): Intended size of each part

	Returns:
		:List: List of list each with subset of the data
	"""
	# this
	groupn = int(len(S) / size + 0.5)
	groups = [[] for i in range(groupn)]

	for i in range(len(S)):
		groups[i % groupn].append(S[i])
	# end for
	return groups
# end def


def getattrs(obj):
	"""Get the attribute of an object

	Args:
		:obj: Input object

	Returns:
		:Callable: callable object to retrieve the value of certain attribute
	"""
	return lambda x: getattr(obj, x)
# end def


def swap(a, b):
	"""Swap two objects

	Args:
		:a: First object
		:b: Second object

	Returns:
		:Tuple: swapped objects
	"""
	return a, b
# end def


# MATH #
def min_max_index(s):
	"""Return the minimum and maximum index and value of the input series

	Args:
		:s: Input series

	Returns:
		:Tuple: ( (min_value, min_index), (max_value, max_index) )
	"""
	sorter = sorted(zip(s, list(range(len(s)))))
	return sorter[0], sorter[-1]
# end def


def min_index(s):
	"""Return the minimum index and value of the input series

	Args:
		:s: Input series

	Returns:
		:Tuple: (min_value, min_index)
	"""
	return min_max_index(s)[0]
# end def


def max_index(s):
	"""Return the maximum index and value of the input series

	Args:
		:s: Input series

	Returns:
		:Tuple: (max_value, max_index)
	"""
	return min_max_index(s)[-1]
# end def


def weighted_average(data, weights):
	"""Compute weighted average of an input series

	Args:
		:data (Series): Input series
		:weights (Series): Weights of each item. must be of the same length with input series

	Returns:
		:Float: weighted average
	"""
	weights_normed = normalize(weights)
	return numpy.dot(weights_normed, data)
# end def


def normalize(data):
	"""Normalize input series

	Args:
		:data: Input series

	Returns:
		:numpy array: Normalized input series
	"""
	assert min(data) >= 0
	return numpy.array(data, dtype=float) / sum(data)
# end def


def normalize_dict(s):
	"""Normalize input dictionary

	Args:
		:s (Dict): Input dictionary

	Returns:
		:Dict: Normalized input dictionary
	"""

	assert type(s) == dict
	return dict(list(zip(list(s.keys()), normalize(list(s.values())))))
# end def


def isint(s):
	"""Check if the input value is an integer

	Args:
		:s: Input value

	Returns:
		:Bool: if the input value is integer
	"""

	if numpy.isinf(s):
		return False
	if numpy.isneginf(s):
		return False
	if s != s:
		return False
	return s - int(s) == 0
# end def


# FILE IO #
def get_file_timestamp(filename):
	"""Get the timestamp of a file

	Args:
		:filename (String): input file path

	Returns:
		:String: timestamp of the file in isoformat
	"""
	return 'T'.join(str(dateutil.parser.parse(time.ctime(os.path.getmtime(filename)))).strip().split())
# end def


def unique_name():
	"""Return string of UUID4
	"""

	return str(uuid.uuid4())
# end def


def unique_filename(suffix='', directory='./'):
	"""Return a unique file name

	Args:
		:suffix: suffix of the file name
		:directory: directory to test duplicate of the proposed file name

	Returns:
		:String: proposed file name
	"""

	temp_filename = directory + os.sep + unique_name() + suffix
	while True:
		if os.path.exists(temp_filename):
			temp_filename = directory + os.sep + unique_name() + suffix
		else:
			break
		# end if
	# end while
	return temp_filename
# end def


def shuffle(s, random_seed=None):
	"""wrapper around random.shuffle. Shuffle a series

	Args:
		:s (Series): Input series to shuffle
		:random_seed: random seed

	Returns:
		:Series: shuffled series
	"""

	if random_seed is not None:
		random.seed(random_seed)
	# end if
	r = copy.copy(s)
	random.shuffle(r)
	return r
# end def


def remove_from_list(s, unwanteds):
	"""Remove specified items from a list

	Args:
		:s (List): Input list
		:unwanteds (Series): items to be excluded from the list

	Returns:
		:List: cleaned up list
	"""

	return [t for t in s if t not in unwanteds]
# end def


def run_parallel_commands(cmds, threads=4):
	"""Execute list of (bash) commands concurrently

	Args:
		:cmds (List): List of command strings
		:threads (Int): number of threads to execute the commands

	Returns:
		:(null)
	"""

	chunks = []
	for i in range(len(cmds)):
		if i % threads == 0:
			chunks.append([cmds[i]])
		else:
			chunks[-1].append(cmds[i])
		# end if
	# end for

	for chunk in chunks:
		for cmd in chunk:
			print(cmd)
		processes = [subprocess.Popen(cmd, shell=True) for cmd in chunk]
		exit_codes = [p.wait() for p in processes]
	# end for
# end def


def file_exists(filename):
	"""Check if a file exists. Wrapper around os utility

	Args:
		:filename: File path to check

	Returns:
		:Bool: if the file exists
	"""

	return os.path.isfile(filename)
# end def


def files_in_directory(directory, suffixes=[], prefixes=[]):
	"""List all files in a directory

	Args:
		:directory (String): Directory to list the files
		:suffixes (Series): suffix filter of the files
		:prefixes (Series): prefix filter of the files

	Returns:
		:List: List of filenames
	"""

	filenames = os.listdir(directory)

	# handler prefix
	if len(prefixes) != 0:
		filenames = [filename for filename in filenames if any([filename.startswith(prefix) for prefix in prefixes])]

	# handler suffix
	if len(suffixes) != 0:
		filenames = [filename for filename in filenames if any([filename.  endswith(suffix) for suffix in suffixes])]

	# put back directory
	filenames = [directory + os.sep + filename for filename in filenames]

	return filenames
# end def


def filter_printable(text):
	"""Filter text to retain only printable part

	Args:
		:text (String): Input text string

	Returns:
		:String: Printable string
	"""

	return ''.join([x for x in text if x in string.printable])
# end def


def compact_text(txt):
	"""To make text more compact by removing unncessary white spaces and tabs

	Args:
		:txt (String): Input text

	Returns:
		:String: clearned text
	"""

	return ' '.join(txt.split()).strip()
# end def


def download_http(url, outfilename=None, timeout=120, mode='w'):
	"""Download a web page to local http file

	Args:
		:url (String): url of the web page
		:outfilename (String): output file name
		:timeout (Float): number of seconds for connection time-out
		:mode (Char): mode of the file handler

	Returns:
		:Dict: keys include ('status', 'type', 'content')
	"""

	assert type(outfilename) == str or outfilename is None

	r = requests.get(url, timeout=timeout)
	if r.status_code == 200:
		if outfilename is not None:
			with open(outfilename, mode) as fout:
				fout.writelines(r.text)
			# end with
			return {'status': 'success', 'type': 'file', 'content': outfilename}
		else:
			return {'status': 'success', 'type': 'text', 'content': r.text}
		# end if
	# end if
# end def

def date_isoformat():
	cur_date = datetime.datetime.now()
	return cur_date.isoformat(sep='T').split('T')[0]
# end def

def timestamp():
	"""Wrapper of datetime.now() and convert to String type

	Args:

	Returns:
		:String: timestamp
	"""

	return str(datetime.datetime.now())
# end def


def utc_timestamp():
	"""Returns a UTC timestamp

	Args:
		: (null)
	Returns:
		:Float
	"""

	return time.mktime(time.gmtime()) + time.time() - int(time.time())
# end def


def mkdir_p(path):
	"""Create a directory if it has not existed

	Args:
		:path (String): path of the directory to create

	Returns:
		:(null)
	"""

	try:
		os.makedirs(path)
	except OSError as exc:
		if exc.errno == errno.EEXIST and os.path.isdir(path):
			pass
		else:
			raise
	# end try
# end def


def create_dir_if_necessary(path):
	"""Create a directory if it has not existed

	Args:
		:path (String): path of the directory to create

	Returns:
		:(null)
	"""

	return mkdir_p(path)
# end def


def tail(filename, n_lines):
	"""Return the last n lines from a file as a list

	Args:
		:filename (String): path of file to retrieve
		:n_lines (Int): number of lines to retrieve

	Returns:
		:List: List of lines
	"""

	assert type(n_lines) == int or n_lines == numpy.inf
	if n_lines == numpy.inf:
		return open(filename).read().splitlines()
	# end if

	cmd = 'tail -'+str(n_lines)+' '+filename
	status, output = subprocess.getstatusoutput(cmd)
	if status != 0:
		return None
	# end if
	output = output.splitlines()
	return output
# end def


# standard exception catching decorator
def std_exc_catch(func):
	"""A decorator to "catch all" exceptions and return as a dictionary

	Args:

	Returns:
		:(decorator has no effect if no exception caught)
		:Dict: keys include ('status', 'data', 'errors')
	"""

	@wraps(func)
	def func_wrapper(*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except Exception as e:
			return {'status': 'failed', 'data': [], 'errors': [repr(e)]}
		# end try
	# end def
	return func_wrapper
# end def


def count_elements(s_in):
	"""Count the number of elements in a Series

	Args:
		:s_in (Series): Input series

	Returns:
		:Dict: Dictionary of key-value pair of number of counts of each element
	"""

	swap = [_ for _ in s_in if _ == _]
	out = dict(collections.Counter(swap))
	na_count = len(s_in) - len(swap)
	if na_count != 0:
		out[np.nan] = na_count

	return out
# end def

def compute_contigency_table(DF, columns):
	"""Compute the 2x2 frequency table of selected columns in a DataFrame

	Args:
		:DF (DataFrame): Input pandas DataFrame
		:columns (List): Selected columns in the input DataFrame. Dimension must be 2.

	Returns:
		:DataFrame: 2x2 frequency table
	"""

	assert len(set(columns)) == 2, 'dim of "columns" must be 2'
	assert set(list(columns)) <= set(list(DF.columns)), 'some input columns not in dataframe'

	col_p, col_q = columns
	# get values only
	data = DF[[col_p, col_q]].values
	data = list(map(tuple, data))
	# count values
	counters = opends.shorthand.count_elements(data)

	col_ps = list(DF[col_p].unique())
	col_qs = list(DF[col_q].unique())

	# initialize table
	tbl = numpy.zeros([len(col_ps), len(col_qs)])

	# fill in values from counter
	for key, val in counters.items():
		p, q = key
		idx_p = col_ps.index(p)
		idx_q = col_qs.index(q)
		tbl[idx_p, idx_q] = val
	# end for
	df = pd.DataFrame(tbl, columns=col_qs, index=col_ps).astype(int)
	return df
# end def

def compute_contigency_table(DF, columns):
	"""Compute the 2x2 frequency table of selected columns in a DataFrame

	Args:
		:DF (DataFrame): Input pandas DataFrame
		:columns (List): Selected columns in the input DataFrame. Dimension must be 2.

	Returns:
		:DataFrame: 2x2 frequency table
	"""

	assert len(set(columns)) == 2, 'dim of "columns" must be 2'
	assert set(list(columns)) <= set(list(DF.columns)), 'some input columns not in dataframe'

	col_p, col_q = columns
	counters = DF[columns].groupby(columns).agg(len).to_dict()

	col_ps = list(DF[col_p].unique())
	col_qs = list(DF[col_q].unique())

	# initialize table
	tbl = numpy.zeros([len(col_ps), len(col_qs)])

	# fill in values from counter
	for key, val in counters.items():
		p, q = key
		idx_p = col_ps.index(p)
		idx_q = col_qs.index(q)
		tbl[idx_p, idx_q] = val
	# end for
	df = pd.DataFrame(tbl, columns=col_qs, index=col_ps).astype(int)
	df.index.name = col_p
	return df
# end def


def calculate_JSD(p, q, n_bins=20, alpha=0.05):
	'''Calculate estimated JS divergence between two data-sets
		:n_bins (Int) : number of bins to make histogram
		:alpha (Float): pseudocount ratio
	'''

	# relative entropy
	lb = min(min(p), min(q))
	ub = max(max(p), max(q))
	bins = numpy.linspace(lb, ub, n_bins+1)
	# make (discretized) distributions
	h1 = numpy.histogram(p, bins=bins)[0]
	h2 = numpy.histogram(q, bins=bins)[0]
	h1 = h1 / sum(h1)
	h2 = h2 / sum(h2)
	# pseudo-count to prevent zero
	alpha = alpha / len(h1)
	h1 += alpha
	h2 += alpha
	h1 = h1 / sum(h1)
	h2 = h2 / sum(h2)
	jsd = 0.5 * (scipy.stats.entropy(h1, h2) + scipy.stats.entropy(h2, h1))

	return jsd
# end def

def choose_majority(s_in, exceptions=None):
	"""Select the majority item in an input series

	Args:
		:s_in: Input series
		:exceptions (Series): exclusion list

	Returns:
		:object: most common object in the input series
	"""

	if exceptions is None:
		exceptions = []
	# end if

	S = [_ for _ in s_in if _ == _]
	t = count_elements(S)
	for e in exceptions:
		if e in list(t.keys()):
			t.pop(e)
	sorter = [(q, p) for p, q in list(t.items())]
	sorter.sort()
	return sorter[-1][1]
# end def


def isnan(s):
	"""Check is the value is NaN

	Args:
		:s: Input value to check

	Returns:
		:Bool: if the value is NaN
	"""

	return not(s == s)
# end def


def require_inputs(requireds):
	"""Decorator to check if the required input dictionary to a function is sufficient

	Args:
		:List/Tuple: Names of required variables

	Returns:
		:Dict: Status, Error and Data of the input test

	Examples:

		>>> @require_inputs(["username", "password", "conn_string"])
			def test_db_conn(input_pars):
			  username = input_pars['username']
			  password = input_pars['password']
			  conn_str = input_pars['conn_string']
			  return
		>>> test_db_conn({'username': 'foo', 'conn_string': 'bar'})
		{'data': [], 'errors': ["missing inputs ['password']"], 'status': 'failed'}
	"""

	def func_decorator(func):
		@wraps(func)
		def func_wrapper(input_pars, **kwargs):
			missings = list(set(requireds) - set(input_pars.keys()))
			if len(missings) != 0:
				return {'status': 'failed', 'errors': ['missing inputs %s' % str(missings)], 'data': []}
			else:
				return func(input_pars, **kwargs)
			# end if
		# end def
		return func_wrapper
	return func_decorator
# end def


# class method version
def cls_require_inputs(requireds):
	"""Decorator (class instance function) to check if the required input dictionary to a function is sufficient

	Args:
		:List/Tuple: Names of required variables

	Returns:
		:Dict: Status, Error and Data of the input test

	Examples:

		>>> class Ham:
			  @cls_require_inputs(["username", "password", "conn_string"])
			  def test_db_conn(self, input_pars):
				username = input_pars['username']
				password = input_pars['password']
				conn_str = input_pars['conn_string']
				return
		>>> ham = Ham()
		>>> ham.test_db_conn({'username': 'foo', 'conn_string': 'bar'})
		{'data': [], 'errors': ["missing inputs ['password']"], 'status': 'failed'}
	"""

	def func_decorator(func):
		@wraps(func)
		def func_wrapper(cls, input_pars, **kwargs):
			missings = list(set(requireds) - set(input_pars.keys()))
			if len(missings) != 0:
				return {'status': 'failed', 'errors': ['missing inputs %s' % str(missings)], 'data': []}
			else:
				return func(cls, input_pars, **kwargs)
			# end if
		# end def
		return func_wrapper
	return func_decorator
# end def


def num2rank(s):
	"""Returns the ranking of each item in a Series

	Args:
		:s: Input series

	Returns:
		:List: Ranking of every item in the series
	"""

	sorter = list(zip(s, list(range(len(s)))))
	sorter.sort()
	sorter2 = [(sorter[i][1], i) for i in range(len(sorter))]
	sorter2.sort()
	return list(zip(*sorter2))[1]
# end def


def meta_parse_args(parser, meta_keyword='meta'):
	"""Wrapper for argparse to support configuration file input
	"""

	# collect all the required variables
	requireds = []
	for _action in parser._actions:
		if type(_action) != argparse._StoreAction:
			continue
		if _action.required:
			requireds.append(_action.dest)
			_action.required = False
		# end if
	# end if

	# parse argument input
	pars = vars(parser.parse_args())
	# load meta parameters files
	meta_pars = {}
	if meta_keyword in list(pars.keys()):
		source_files = pars[meta_keyword]
		if source_files is None:
			pass
		elif type(source_files) in [list, tuple]:
			for source_file in source_files:
				meta_pars.update(yaml.load(open(source_file)))
			# end for
		else:
			meta_pars.update(yaml.load(open(source_files)))
		# end if
	# end if

	# overwrite with argument input
	pars.update(meta_pars)

	# check for mandatory parameters
	keys = [key for key, val in list(pars.items()) if val is not None]
	_missings = list(set(requireds) - set(keys))
	try:
		if len(_missings) != 0:
			parser.print_help()
			raise ValueError('Error: missing arguments: %s are required.' % str(_missings))
		# end if
	except Exception as e:
		sys.exit(e)
	# end try

	return pars
# end def


class ConfigurationHandler:
	Pars = {}

	def __init__(self, source_type, source_path=None):
		'''configuration variable holder and handler. Now only supports flat file types (json, yaml)'''
		# ensure file exists
		if source_type.endswith('file') and file_exists(source_path) is False:
			msg = 'source file %s not exists' % (source_path)
			logger.error(msg)
			raise ValueError(msg)
		# end if

		self.source_type = source_type
		self.source_path = source_path

		# actually read the parameters
		self.refresh(mode='update')
	# end def

	def read_source(self):
		pars = None
		if self.source_type == 'json':
			pars = json.load(open(self.source_path))
		elif self.source_type == 'yaml':
			pars = yaml.load(open(self.source_path))
		# end if
		return pars
	# end def

	def refresh(self, mode='update'):
		# refresh the parameter from sources
		# mode: update -> add any new prarameters; renew: complete renewal

		# check input validity
		assert self.source_type in ['json', 'yaml']
		assert mode in ['update', 'renew'], 'error: mode %s not understood' % (mode)

		# read from source
		try:
			pars = self.read_source()
		except Exception as e:
			logger.error('reading from source failed: ' + repr(e))
			return
		# end try

		# logging out if parameters change
		if self.Pars == pars:
			logger.info('No change to parameters')
		else:
			logger.info('Some changes to parameters')
		# end if

		# actually update the parameters
		if mode == 'update':
			self.Pars.update(pars)
		elif mode == 'renew':
			self.Pars = pars
		# end if

	# end def
# end class


class ThresholdNameClassifier:

	def __init__(self, thresholds, names):
		"""A classifier to classify and label object based on their value

		Args:
			:thresholds (Series): threshold values for every class
			:names (string): must be of length of thresholds series + 1

		Returns:
			:(null)
		"""

		thresholds = list(sorted(list(thresholds)))
		assert len(names) - len(thresholds) == 1

		self.thresholds = [-numpy.inf] + thresholds + [numpy.inf]
		self.names = names
	# end def

	def parse(self, values):
		assert isinstance(values, str) is False
		if not isinstance(values, collections.Iterable):
			values = [values]
		# end if

		output = []
		for value in values:
			name = None
			for i in range(len(self.thresholds)-1):
				if value <= self.thresholds[i+1]:
					name = self.names[i]
					break
				# end if
			# end for
			output.append(name)
		# end for

		return output
	# end def
# end class


class timeout:

	def __init__(self, secs=30):
		"""A context manager to handler timeout exception

		Args:
			 :secs (Float): number of secs before terminating the context

		Returns:
			:(null)
		"""

		self.secs = secs
	# end def

	def handle_timeout(self, signum, frame):
		raise TimeoutError('Timed out after %d seconds' % (self.secs,))
	# end def

	def __enter__(self):
		signal.signal(signal.SIGALRM, self.handle_timeout)
		signal.alarm(self.secs)
	# end def

	def __exit__(self, type, value, traceback):
		signal.alarm(0)
	# end def
# end class


# note: taken and modified from https://stackoverflow.com/questions/21786382/pythonic-way-of-retry-running-a-function
class RetryException(Exception):
	u_str = "Exception ({}) raised after {} tries."

	def __init__(self, exp, max_retry):
		"""An exception class to manager retry failure

		Args:
			:exp (String): expression text template
			:max_retry (Int): number of maximum retry reached
		"""

		self.exp = exp
		self.max_retry = max_retry
	# end def

	def __unicode__(self):
		return self.u_str.format(self.exp, self.max_retry)
	# end def

	def __str__(self):
		return self.__unicode__()
	# end def
# end class


def retry_func(max_retry=5, wait_secs=0):
	"""A decorator to retry function call automatically after waiting for a few secs

	Args:
		:max_retry (Int): number of times to retry
		:wait_secs (Float): number of seconds to wait before retry

	Returns:
		:Func
	"""

	def func_decorator(func):
		@wraps(func)
		def func_wrapper(*args, **kwargs):
			err = None
			for retry in range(1, max_retry + 1):
				try:
					return func(*args, **kwargs)
				except Exception as e:
					time.sleep(wait_secs)
					err = e
				# end try
			# end for
			raise RetryException(err, max_retry)
		# end def
		return func_wrapper
	return func_decorator
# end def

class Struct():
	def __init__(self, _dict):
		"""Construct an object initialized attributes with a dictionary
		Args:
			:_dict (Dict): object attributes to be initialized

		Returns:
			:Object
		"""

		self.__dict__.update(_dict)
	# end def

# end class

class EmptyObj(Struct):
	pass
# end def

# taken from https://stackoverflow.com/questions/7406102/create-sane-safe-filename-from-any-unsafe-string
def gen_safe_filename(filename, keepcharacters=('.', '_', '-', '/')):
	"""Returns a syntactically safe filename

	Args:
		:filename (String): Intended file name
		:keepcharacters (Series): Any non-alphanumeric character to keep in the file name

	Returns:
		:String: safe file name
	"""

	return ''.join(c for c in filename if c.isalnum() or c in keepcharacters).rstrip()
# end def

def jinja_render_template(html, context):
	"""Rendering a file using jinja engine

	Args:
		:tpl (String): template html text
		:context (Dict): context for jinja engine to parse

	Returns:
		:String: rendered content
	"""

	return jinja2.Environment(loader=jinja2.BaseLoader).from_string(html).render(context)
# end def

# taken from http://matthiaseisen.com/pp/patterns/p0198/
def jinja_render(tpl_path, context):
	"""Rendering a file using jinja engine

	Args:
		:tpl (String): template file (usually html)
		:context (Dict): context for jinja engine to parse

	Returns:
		:String: rendered content
	"""

	path, filename = os.path.split(tpl_path)
	return jinja2.Environment(
		loader=jinja2.FileSystemLoader(path or './')
	).get_template(filename).render(context)
# end def


class SimpleObfuscator:
	def __init__(self):
		self.universe = '%(q2CJOkv1R&5#4zhn3A+-iZr,HS^D)0w\\Tb9]fEIB!6|?=Xsxgm_[:*VLl`\'"@j<a;t7 8}FM$pU>Qd;YKNe/~P{c.oWGuy'
	# end def

	def encrypt(self, plaintext, key):
		assert set(list(plaintext)) <= set(self.universe), 'unobfuscatable characters in plaintext detected'
		assert set(list(key))       <= set(self.universe), 'unobfuscatable characters in key detected'
		enc = self._encrypt(plaintext, key)
		return urllib.parse.quote(enc) 
	# end def

	def decrypt(self, ciphertext, key):
		txt = urllib.parse.unquote(ciphertext)
		return self._decrypt(txt, key)
	# end def

	def _encrypt(self, plaintext, key):
		key_length = len(key)
		key_as_int = [self.universe.index(i) for i in key]
		plaintext_int = [self.universe.index(i) for i in plaintext]
		ciphertext = ''
		for i in range(len(plaintext_int)):
			value = (plaintext_int[i] + key_as_int[i % key_length]) % len(self.universe)
			ciphertext += self.universe[value]
		return ciphertext
	# end def


	def _decrypt(self, ciphertext, key):
		key_length = len(key)
		key_as_int = [self.universe.index(i) for i in key]
		ciphertext_int = [self.universe.index(i) for i in ciphertext]
		plaintext = ''
		for i in range(len(ciphertext_int)):
			value = (ciphertext_int[i] - key_as_int[i % key_length]) % len(self.universe)
			plaintext += self.universe[value]
		return plaintext
	# end def
# end class


# taken from https://stackoverflow.com/questions/12524994/encrypt-decrypt-using-pycrypto-aes-256
class SimpleEncryptor():

	def __init__(self):
		"""Construct a simple encryptor
		"""
		self.bs = 32
	# end def

	def simple_encrypt(self, msg, secret_key):
		"""Enrypt a message with a (symmetric) secret key

		Args:
			:msg (String): message to be encrypted
			:secret_key (String): secret key

		Returns:
			:Bytes
		"""

		from Crypto.Cipher import AES
		from Crypto import Random

		raw = msg
		key = secret_key
		key = hashlib.sha256(key.encode()).digest()

		raw = self._pad(raw)
		iv = Random.new().read(AES.block_size)
		cipher = AES.new(key, AES.MODE_CBC, iv)
		return base64.b64encode(iv + cipher.encrypt(raw))

	def simple_decrypt(self, msg, secret_key):
		"""Decrypt a message with a (symmetric) secret key

		Args:
			:msg: (encrypted) message to be decrypted
			:secret_key (String): secret key

		Returns:
			:String: Decrypted message
		"""

		from Crypto.Cipher import AES
		from Crypto import Random

		enc = msg
		key = secret_key
		key = hashlib.sha256(key.encode()).digest()

		enc = base64.b64decode(enc)
		iv = enc[:AES.block_size]
		cipher = AES.new(key, AES.MODE_CBC, iv)
		return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

	def _pad(self, s):
		"""add pads to message
		"""

		return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

	@staticmethod
	def _unpad(s):
		"""remove pads from message
		"""

		return s[:-ord(s[len(s)-1:])]
# end class


# taken from https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
def decode_base64(data):
	"""Decode base64, padding being optional.

	Args:
		:data (bytes): Input data

	Returns:
		:encoded data
	"""
	missing_padding = len(data) % 4
	if missing_padding != 0:
		data += b'='*(4 - missing_padding)
	return base64.decodebytes(data)
# end def


def split_sublist(S, n_parts):
	"""Split an input series into N number of parts

	Args:
		:S: Input series
		:n_part (int): number of parts

	Returns:
		:List: each item is a list with subset of the input data
	"""

	# split into chunks
	nchunks = int(len(S) / float(n_parts)) + 1
	chunks = [S[x:x+nchunks] for x in range(0, len(S), nchunks)]

	# ensure no loss of data
	assert sum(list(map(len, chunks))) == len(S)
	return chunks
# end def


# "adding" two dictionary
def add_dicts(dict_p, dict_q):
	"""Adding two dictionaries together

	Args:
		:dict_p (Dict): First dictionary object
		:dict_q (Dict): Second dictionary object

	Returns:
		:Dict: combined dictionary
	"""

	swap = copy.deepcopy(dict_p)
	for key, val in dict_q.items():
		if key in swap.keys():
			swap[key] += val
		else:
			swap[key] = val
		# end if
	# end for
	return swap
# end def


def get_ranking(s, ascending=False):
	"""Returns the ranking of each item in a Series

	Args:
		:s: Input series
		:ascending (Bool): whether to rank the items in an ascending order

	Returns:
		:List: Ranking of every item in the series
	"""

	ranks = num2rank(s)
	if ascending is False:
		ranks = list(reversed(ranks))
	# end if
	return ranks
# end def



# find original file (resolve links)
def resolve_link(curfile):
	while True:
		if os.path.islink(curfile):
			curfile = os.readlink(curfile)
			curfile = os.path.abspath(curfile)
		else:
			break
		# end if
	# end while
	return curfile
# end def

# copied and modified from https://stackoverflow.com/questions/51359783/python-flatten-multilevel-nested-json
def flatten_json(y, sep='.'):
	out = {}

	def flatten(x, name=''):
		if type(x) is dict:
			for a in x:
				flatten(x[a], name + a + sep)
		elif type(x) is list:
			i = 0
			for a in x:
				flatten(a, name + str(i) + sep)
				i += 1
		else:
			out[name[:-1]] = x
		# end if
	# end def

	flatten(y)
	return out
# end def
