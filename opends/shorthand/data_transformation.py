import opends
import numpy as np
import scipy

def recommend_log_transform(s, outlier_thr_pcts = [5, 95], entropy_red_thr=0.3):
	'''Recommend whether to log-transform the feature'''
	assert len(outlier_thr_pcts) == 2, 'please provide lower and upper bound values for outlier cut-off'
	assert min(outlier_thr_pcts) >= 0 and max(outlier_thr_pcts) <=100, 'percentilev values must be between [0, 100]'
	# handle outlier, cut away extreme values from both sides
	lb, ub = np.percentile(s, outlier_thr_pcts)
	assert lb < ub, 'lower bound must be smaller than upper bound'
	vals = np.array([_ for _ in s if lb < _ < ub])
	
	def cal_rel_norm_entropy(vals, npoints=20):
		'''Calculate relative entropy to expected normal distribution'''
		_stats = opends.stats.simple_stats(vals)
		u, s = _stats['median'], _stats['IQR'] / 1.349
		bins = np.linspace(_stats['min'], _stats['max'], npoints+1)
		
		# build discretized distribution
		ys = np.histogram(vals, bins=bins)[0]
		ys = ys/sum(ys)
		# build expected normal distribution
		xs = (bins[1:] + bins[:-1]) / 2.
		f = scipy.stats.norm(loc=u, scale=s)
		y_norms = f.pdf(xs)
		y_norms = y_norms / sum(y_norms)
		
		# calculate entropy
		return scipy.stats.entropy(ys, y_norms)
	# end def
	
	# original entropy
	entropy_p = cal_rel_norm_entropy(vals)
	# transformed entropy
	vals = np.log(vals - min(vals) + 1)
	entropy_q = cal_rel_norm_entropy(vals)
	
	# if the entropy reduction satisfies the threshold
	return entropy_q < entropy_p*(1-entropy_red_thr)
# end def

