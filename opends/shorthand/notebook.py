import os
import re
import json
import pygit2
import requests
import ipykernel
from pathlib import Path
from requests.compat import urljoin
import logging
logger = logging.getLogger()

try:  # Python 3 (see Edit2 below for why this may not work in Python 2)
	from notebook.notebookapp import list_running_servers
except ImportError:  # Python 2
	import warnings
	from IPython.utils.shimmodule import ShimWarning
	with warnings.catch_warnings():
		warnings.simplefilter("ignore", category=ShimWarning)
		from IPython.html.notebookapp import list_running_servers
	# end with
# end try

# copied from https://github.com/jupyter/notebook/issues/1000
def get_notebook_name():
	"""
	Return the full path of the jupyter notebook.
	"""
	kernel_id = re.search('kernel-(.*).json',
						  ipykernel.connect.get_connection_file()).group(1)
	servers = list_running_servers()

	for ss in servers:
		# TODO: better catchment of errors
		try:
			response = requests.get(urljoin(ss['url'], 'api/sessions'),
									params={'token': ss.get('token', '')})
			for nn in json.loads(response.text):
				if nn['kernel']['id'] == kernel_id:
					relative_path = nn['notebook']['path']
					return os.path.join(ss['notebook_dir'], relative_path)
				# end if
			# end for
		except:
			pass
		# end try
	# end for
# end def

def get_current_ref():
	# get the current repo
	git_dir  = pygit2.discover_repository(os.environ['PWD'])
	if git_dir:
		project_root = str(Path(git_dir).parent)
		repo = pygit2.Repository(project_root)
		git_info = {
		  'repo_origin_url': repo.remotes['origin'].url,
		  'branch_name'    : repo.head.shorthand
		} # end info
	else:
		logger.warning('Git directory cannot be found')
		project_root = os.environ['HOME']
		git_info = None
	# end if
		
	try:
		filepath = get_notebook_name()
	except RuntimeError: # TODO: better catchment
		logger.warning('Not a running jupyter notebook')
		filepath = os.path.abspath(inspect.getfile(inspect.currentframe()))
	# end try

	relpath = os.path.relpath(filepath, project_root)

	output = {
	  'git'     : git_info,
	  'filepath': relpath,
	} # end output
	return output
# end def







