import os
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.audio import MIMEAudio
from email.mime.image import MIMEImage
from email.encoders import encode_base64

def sendmail_smtp(sender_addr, recipient_addr, smtp_server, smtp_port, smtp_username, smtp_password, subject, text, attachmentFilePaths):

	def getAttachment(attachmentFilePath):
		contentType, encoding = mimetypes.guess_type(attachmentFilePath)

		if contentType is None or encoding is not None:
			contentType = 'application/octet-stream'

		mainType, subType = contentType.split('/', 1)
		file = open(attachmentFilePath, 'rb')

		if mainType == 'text':
			attachment = MIMEText(file.read())
		elif mainType == 'message':
			attachment = email.message_from_file(file)
		elif mainType == 'image':
			attachment = MIMEImage(file.read(),_subType=subType)
		elif mainType == 'audio':
			attachment = MIMEAudio(file.read(),_subType=subType)
		else:
			attachment = MIMEBase(mainType, subType)
		attachment.set_payload(file.read())
		encode_base64(attachment)

		file.close()

		attachment.add_header('Content-Disposition', 'attachment',	 filename=os.path.basename(attachmentFilePath))
		return attachment
	# end def
	msg = MIMEMultipart()
	msg['From'] = sender_addr
	msg['To'] = recipient_addr
	msg['Subject'] = subject
	msg.attach(MIMEText(text))

	assert type(attachmentFilePaths) in (list, tuple), 'attachmentFilePaths must be of list or tuple type'
	for attachmentFilePath in attachmentFilePaths:
		msg.attach(getAttachment(attachmentFilePath))

	mailServer = smtplib.SMTP(smtp_server, smtp_port)
	mailServer.ehlo()
	mailServer.starttls()
	mailServer.ehlo()
	mailServer.login(smtp_username, smtp_password)
	mailServer.sendmail(sender_addr, recipient_addr, msg.as_string())
	mailServer.close()

# end def

def sendmail_unix(emailUser, recipient, subject, text, attachFile = None):

	fp = tempfile.NamedTemporaryFile()
	fp.write(text)
	fp.seek(0)

	cmd_template = '''mail -aFrom:{emailUser} -s "{subject}" --attach "{attachFile}" {recipient} < {textFile}'''
	cmd = cmd_template.format(
		emailUser = emailUser,
		recipient = recipient,
		subject   = subject,
		textFile  = fp.name,
		attachFile = attachFile
	)

	if attachFile == None:
		cmd = cmd.replace('--attach "None"', '')
	# end if

	process = subprocess.Popen(cmd, shell = True)
	process.wait()
# end def

# send email with sendGrid (warning: use API key here, need more consideration on its security)
def sendmail_sendgrid(emailUser, recipient, api_key, subject, text, mimetype='text/plain', attachmentFilePaths = []):
	assert mimetype in ['text/plain', 'text/html'], 'unsupported mimetype:'+mimetype
	import base64
	import sendgrid
	import sendgrid.helpers.mail

	def getAttachment(attachmentFilePath):
		attachment = sendgrid.helpers.mail.Attachment()

		# guess type
		contentType, encoding = mimetypes.guess_type(attachmentFilePath)

		# fill up information
		attachment.content     = base64.b64encode(open(attachmentFilePath, 'rb').read()).decode()
		attachment.type        = contentType
		attachment.filename    = os.path.basename(attachmentFilePath)
		attachment.disposition = "attachment"
		return attachment
	# end def

	# connect to the server
	sg = sendgrid.SendGridAPIClient(api_key)

	mail = sendgrid.helpers.mail.Mail()
	email = sendgrid.helpers.mail.Email()
	email.email = emailUser
	mail.from_email = email
	mail.subject = subject
	
	personalization = sendgrid.helpers.mail.Personalization()
	for address in recipient.split(','):
		personalization.add_to(sendgrid.helpers.mail.Email(address.strip()))
	# end for
	mail.add_personalization(personalization)
	mail.add_content(sendgrid.helpers.mail.Content(mimetype, text))

	# add attachments
	for attachmentFilePath in attachmentFilePaths:
		mail.add_attachment(getAttachment(attachmentFilePath))
	# end for

	# actually send
	response = sg.client.mail.send.post(request_body=mail.get())
	return response
# end def

def sendmail_postmark(emailUser, recipient, api_key, subject, text, mimetype='text/plain', attachmentFilePaths = []):
	assert mimetype in ['text/plain', 'text/html'], 'unsupported mimetype:'+mimetype
	import postmarker.core

	postmark = postmarker.core.PostmarkClient(server_token=api_key)
	kwargs = {
	  'From'       : emailUser,
	  'To'         : recipient,
	  'Subject'    : subject,
	} # kwargs

	if attachmentFilePaths != []:
		kwargs['Attachments'] = attachmentFilePaths
	# end if
	if mimetype == 'text/plain':
		kwargs['TextBody'] = text
	elif mimetype == 'text/html':
		kwargs['HtmlBody'] = text
	# end if

	return postmark.emails.send(**kwargs)
# end def

def send_mail(provider='sendgrid', **kwargs):
	if   provider == 'sendgrid':
		return sendmail_sendgrid(**kwargs)
	elif provider == 'postmark':
		return sendmail_postmark(**kwargs)
	elif provider == 'smtp':
		return sendmail_smtp(**kwargs)
	elif provider == 'unix':
		return sendmail_unix(**kwargs)
	else:
		raise ValueError('provider "%s" not supported' % provider)
	# end if
# end def

# legacy
send_email = send_mail



