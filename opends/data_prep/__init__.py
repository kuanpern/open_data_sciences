# NOTE: THIS WILL BE MIGRATED TO BRAINLESSML REPOSITORY
import re
import pandas as pd
import collections
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.automap import automap_base
import sqlalchemy.sql.expression
import eralchemy
from eralchemy import render_er
import logging


def camel_case_split(str):
	start_idx = [i for i, e in enumerate(str) if e.isupper()] + [len(str)]
	start_idx = [0] + start_idx
	return [str[x: y] for x, y in zip(start_idx, start_idx[1:])]
# end def

def primitive_colname(s):
	# underscore split
	s = ' '.join(s.replace('_', ' ').strip().split())
	# camel case split
	s = ' '.join(camel_case_split(s))
	return s
# end def


def handle_include_exclude(all_keys, includes=None, excludes=None, logger=None):
	if includes is None and excludes is None:
		return all_keys
	# end if

	if includes is not None and excludes is not None:
		raise ValueError('only one of "includes" or "excludes" should be specified')

	if includes is not None:
		missing_keys = list(set(includes) - set(all_keys))
		if len(missing_keys) != 0:
			if logger is not None:
				logger.warning('Missing: %s' % str(missing_keys))
			# end if
		# end if
	else:
		includes = all_keys
	# end if

	if excludes is not None:
		missing_keys = list(set(excludes) - set(all_keys))
		if len(missing_keys) != 0:
			if logger is not None:
				logger.warning('Not found, so not excluded: %s' % str(missing_keys))
			# end if
		# end if
	else:
		excludes = []
	# end if
	sel_keys = (set(all_keys) & set(includes)) - set(excludes)
	return sel_keys
# end def


class DataDictHandler:
	def __init__(self, logger=logging.getLogger(__name__)):
		"""Initialize a DataDictHandler instance

		Args:
			:logger: logger for the handler to use

		Returns:
			:object: a DataDictHandler instance
		"""

		self.logger = logger

	# end def

	def sample_sql_tables(self, sqlalchemy_engine, n_sample=20, random_seed=None, include_tables=None, include_columns=None, exclude_tables=None, exclude_columns=None):
		"""Randomly sample rows from SQL database tables

		Args:
			:sqlalchemy_engine: SQLAlchemy engine instance
			:n_sample: number of rows to sample
			:random_seed: random seed
			:include_tables: tables to include. None means use all tables.
			:include_columns: columns to include. Incompatible with `include_tables` arguments.
			:exclude_tables: tables to exclude. None means use all tables.
			:exclude_columns: columns to exclude. Incompatible with `include_tables` arguments.

		Returns:
			:List of DataFrames: each dataframe corresponds to a table

		"""
		# set random seed
		if random_seed is not None:
			sqlalchemy.sql.expression.setseed(random_seed)
		# end if

		# create a DB session
		Session = sessionmaker(bind=sqlalchemy_engine)
		session = Session()

		# get metadata using sqlalchemy utility
		metadata = sqlalchemy.MetaData(bind=sqlalchemy_engine)
		metadata.reflect()

		# select tables
		all_table_names = list(metadata.tables.keys())
		table_names = handle_include_exclude(all_table_names, include_tables, exclude_tables, logger=self.logger)

		record = []
		for name in table_names:
			# randomly query rows from the table
			_tbl = metadata.tables[name]
			query = session.query(_tbl).order_by(sqlalchemy.sql.expression.func.random()).limit(n_sample)
			colnames = [_.name for _ in list(metadata.tables[name].columns)]
			colnames = handle_include_exclude(colnames, include_columns, exclude_columns)

			# make into dataframe
			data = [[getattr(_, colname) for colname in colnames] for _ in query]
			_df = pd.DataFrame(data, columns=colnames)

			record.append({'name': name, 'dataframe': _df})
		# end for
		session.close()
		return record

	# end def

	def init_sql_datadict_tables(self, sqlalchemy_engine, er_diagram_output, mode='auto', include_tables=None, include_columns=None, exclude_tables=None, exclude_columns=None, schema=None):
		"""Connect to SQL database and initialize data dictionary tables

		Args:
			:sqlalchemy_engine: SQLAlchemy engine instance
			:er_diagram_output: ER diagram output file. See help(eralchemy.render_er) for documentation
			:(other): for mode, include_tables, include_columns, exclude_tables, exclude_columns, schema, see help(eralchemy.render_er) for doc

		Returns:
			:List of DataFrames: each dataframe corresponds to a data-dictionary table
		"""

		# get metadata using sqlalchemy utility
		metadata = sqlalchemy.MetaData(bind=sqlalchemy_engine)
		metadata.reflect()

		# render to PNG
		eralchemy.render_er(
			input  = metadata,
			output = er_diagram_output,
			# all other parameters
			mode=mode, include_tables=include_tables, include_columns=include_columns, exclude_tables=exclude_tables,
			exclude_columns=exclude_columns, schema=schema
		)

		# build data dictionary for selected tables
		all_table_names = list(metadata.tables.keys())
		table_names = handle_include_exclude(all_table_names, include_tables, exclude_tables, logger=self.logger)

		# actually extract data
		record = []
		for name in table_names:
			tbl = metadata.tables[name]
			# handle include and exclude column name
			all_column_names = tbl.columns.keys()
			column_names = handle_include_exclude(all_column_names, include_columns, exclude_columns)

			# actually handling the table
			record.append({
				'name'     : name,
				'dataframe': self._handle_table(tbl, column_names)
			})
		# end for
		return record
	# end def

	def _handle_table(self, tbl, column_names):
		columns = dict(tbl.columns)

		temp = []
		for col in columns.values():
			_item = {
				'name'          : col.name,
				'desc'          : primitive_colname(col.name),
				'is_primary_key': col.primary_key,
				'is_nullable'   : col.nullable,
				'python_type'   : col.type.python_type.__name__,
				'foreign_keys'  : col.foreign_keys,
				'updated_at'    : None,
				'updated_by'    : None
			} # end _item
			temp.append(_item)
		# end for
		return pd.DataFrame(temp)
	# end def
# end class
