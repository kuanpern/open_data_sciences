import anytree
import anytree.importer
import anytree.exporter

class new_anytree_instance:
	def __init__(self):
		'''Create a new anytree instance with data attribute'''

		# build tree by initialize root
		self.tree = {}
		self.data = {}
		self.metadata = {}

		self.root = anytree.AnyNode(id="root", name="root", data=[])
		self.tree['root'] = self.root
	# end def


	def clear_node_data(self):
		"""clear all node data

		Returns:
			:None
		"""

		for node_id in self.tree.keys():
			_node = self.tree[node_id]
			_node.data = []
		# end for
	# end def

	def trace_ancestors(self, node):
		"""Return list of ancestor nodes of the input node

		Args:
			:node (Anytree node): node to trace ancestory with

		Returns:
			:List of node
		"""
		output = [node]
		while True:
			if node.parent is None:
				break
			# end if
			output.append(node.parent)
			node = node.parent
		# end while
		output.reverse()
		return output
	# end def

	def add_path_to_tree(self, path, force_update=False):
		"""add node to tree based on its path

		Args:
			:path (List of String): List containing the ID of the nodes to add to tree

		Returns:
			:None
		"""
		warnings = []

		path = ['root'] + list(path)

		for i in range(1, len(path)):
			parent_id = path[i-1]
			_id		  = path[i]
			# create node in tree
			if _id in self.tree:
				if self.tree[_id].parent.id != parent_id:
					if force_update is False:
						err_msg = 'child (%s), old-parent (%s), new-parent (%s)' % (_id, self.tree[_id].parent.id, parent_id,)
						raise AssertionError('parent conflict with existing tree: '+err_msg)
					else:
						# force update --> change node's parent
						warnings.append('Changing parent node (%s): %s --> %s)' % (_id, self.tree[_id].parent.id, parent_id,))
						self.tree[_id].parent = self.tree[parent_id]
					# end if
				# end if

				# node already in tree, no need to add new node
				continue
			# end if

			# create node
			_node = anytree.AnyNode(
				id     = _id,
				parent = self.tree[parent_id], # note: must ensure parent created already
				data   = []
			) # end node
			# add to tree
			self.tree[_id] = _node
		# end for
		return self.tree, warnings
	# end def

	def export_tree(self, outfile=None):
		"""export tree to file

		Args:
			:outfile (String): output file path

		Returns:
			:None
		"""

		# export to json text
		exporter = anytree.exporter.JsonExporter(indent=2, sort_keys=True)
		root = self.tree['root']
		out_txt = exporter.export(root)
		if outfile is None:
			return out_txt
		else:
			# write to file
			with open(outfile, 'w') as fout:
				fout.write(out_txt)
			# end with
		# end if
	# end def


	def import_tree(self, infile=None):
		return self.import_tree_from_file(infile=infile)
	# end def

	def import_tree_from_json(self, json_string):
		"""Import tree from file

		Args:
			:json_string (String): input json string

		Returns:
			:None
		"""
		
		importer = anytree.importer.JsonImporter()
		self.root = importer.import_(json_string)

		self.tree = {}
		for node in anytree.PreOrderIter(self.root):
			self.tree[node.id] = node
		# end for
	# end def

	def import_tree_from_file(self, infile=None):
		"""Import tree from file

		Args:
			:infile (String): input file path (.json)

		Returns:
			:None
		"""
		
		importer = anytree.importer.JsonImporter()
		with open(infile, 'r') as fin:
			self.root = importer.read(fin)
		# end with

		self.tree = {}
		for node in anytree.PreOrderIter(self.root):
			self.tree[node.id] = node
		# end for
	# end def
	
	def remove_node(self, node_id):
		"""Remove a node from tree

		Args:
			:node_id (String): ID of the node to remove

		Returns:
			:None
		"""

		_node = self.tree[node_id]
		_node.parent = None
		return self.tree.pop(node_id)
	# end def

# end class
