"""  
Statistics-related Functions
"""

import random
import collections
import numpy
import scipy.stats
import itertools
from minepy import MINE
import logging
logger = logging.getLogger()
from opends import maths
from opends import shorthand
from .distributions import *

def simple_stats(s, dropna=True):
    """Returns basic statistical measures of the input series

    Args:
        :s: input series

    Returns:
        :Dict: simple statistical meaures of the input series
        :dropna (Bool): whether is drop NAs in the series
    """

    if dropna is True:
        swap = [_ for _ in s if _ == _]
    s = swap

    ans = {
      'N'     : len(s),
      'sum'   : sum(s),
      'min'   : numpy.min(s),
      'max'   : numpy.max(s),
      'mean'  : numpy.mean(s),
      'median': numpy.median(s),
      'stdev' : numpy.std(s),
      'IQR'   : IQR(s),
      'skewness': scipy.stats.skew(s),
      'kurtosis': scipy.stats.kurtosis(s)
    }

    # other percentiles
    cuts = numpy.array([10, 30, 70, 90])
    vals = numpy.percentile(s, cuts)
    labels = [str(_)+'%' for _ in cuts]
    ans.update(dict(zip(labels, vals)))

    return ans
# end def


def extract_continuous_segments(s):
    """Returns a summary Construct crossvalidation dataset while maintaining the composition of True and False

    Args:
        :DF: input pandas dataframe
        :label_name (string): column name for binary label
        :n_cv: number of cross-validation data sets
        :random_seed: random seed

    Returns:
        :List: [{'training': dataframe, 'testing': dataframe}, ...]
    """

    output = [[(0, s[0])]]
    for i in range(1, len(s)):
        if s[i] == output[-1][-1][1]:
            output[-1].append((i, s[i]))
        else:
            output.append([(i, s[i])])
        # end if
    # end for

    w = []
    for segment in output:
        w.append([(segment[0][0], segment[-1][0]), segment[0][1]])
    # end for
    return w
# end def


def quantile(data):
    """Returns the quantiles of the input data

    Args:
        :data (Series): input data series

    Returns:
        :List (Float): 25th, 50th and 75th percentile
    """

    cuts = numpy.array([25, 50, 75])
    return numpy.percentile(data, cuts)
# end def


def IQR(s):
    """compute the interquartile range for a distribution S

    Args:
        :s (Series): input data series

    Returns:
        :Float: interquantile range
    """
    if len(s) == 0:
        return numpy.nan
    quantiles = quantile(s)
    return quantiles[2] - quantiles[0]
# end def


def select_range(s, sigma=2.5):
    """return the interval with +-sigma. Useful for outlier filtering

    Args:
        :s (Series): input data series
        :sigma (float): number of standard deviation to include

    Returns:
        :min_value, max_value
    """

    stdev_robust = IQR(s)/1.349
    mean_robust = numpy.median(s)
    r = sigma*stdev_robust
    return mean_robust-r, mean_robust+r
# end def


def weighted_avg_and_std(values, weights):
    """Return the weighted average and standard deviation.

    Args:
        :values: Input values
        :weights: Numpy ndarrays with the same shape.
    Returns:
        :mean, stdev
    """

    ave = numpy.average(values, weights=weights)
    variance = numpy.average((values-ave)**2, weights=weights)  # Fast and numerically precise
    return ave, numpy.sqrt(variance)
# end def


def discretize_distribution(s, n=5):
    """Discretize input series into roughly equally populated classes

    Args:
        :s: input series
        :n: number of classes

    Returns:
        :Series of labels
    """

    return maths.equal_size_binning(s, n=5, labels=None)
# end def


# building roulette wheel
class RouletteWheel:
    def __init__(self, fitness):
        """Construct a Roulette Wheel object for sampling purpose

        Args:
            :fitness (Dict): probability of choosing an object
        """

        assert all(numpy.array(list(fitness.values())) > 0), 'fitness function must be all positive'
        self.fitness = fitness

        # normalize_dict the fitness score
        self.fitness_ = shorthand.normalize_dict(self.fitness)
        self.slots, values = list(zip(*list(self.fitness_.items())))

        self.wheel = list(itertools.accumulate(values))
    # end def

    def slot(self, d):
        """which slot will d falls into
        """

        assert 0 <= d <= 1, 'input value must be in [0, 1]'
        for i in range(len(self.wheel)):
            if d < self.wheel[i]:
                return self.slots[i]
            # end if
        # end for
        return self.slots[-1]
    # end def

    def get(self):
        """Sample an object
        """

        return self.slot(random.random())
    # end def
# end class


# cumulative plot
def cum_plot(s, xtics=numpy.arange(0, 101), matplot=True):
    """Cumulatative plot of input data

    Args:
        :s: input series
        :xtics: percentiles to sample the series
        :matplot (bool): whether to plot using matplotlib library

    Returns:
        :x-coordinates, y-coordinates
    """

    assert 0 <= min(xtics) <= max(xtics) <= 100, 'xtics must be [0, 100]'
    ytics = numpy.percentile(s, xtics)

    if matplot is True:
        import matplotlib.pyplot as plt
        plt.cla()
        plt.plot(ytics, xtics)
        plt.xlabel('value')
        plt.ylabel('percentile')
    # end if

    return xtics, ytics
# end def


def compute_confusion_matrix(predictions, labels):
    """Returns a confusion matrix based on predictions and labels

    Args:
        :predictions (Series): input Boolean list of predictions
        :labels (Series): input Boolean list of actual labels

    Returns:
        :Dict: keys include (TP, FP, TN, FN,)
    """

    out = collections.defaultdict(lambda: 0)
    swap = shorthand.count_elements(list(zip(predictions, labels)))
    out.update(swap)
    output = {
        'TP': out[(True , True)],
        'FP': out[(True , False)],
        'TN': out[(False, False)],
        'FN': out[(False, True)]
    }
    return output
# end def


def binary_classification_assessment(c):
    """Returns summary statistical assessment of a confusion matrix

    Args:
        :c (Dict): confusion matrix, keys must include (TP, FP, TN, FN)

    Returns:
        :Dict: 17 assessment parameters
    """
    assert set(['TP', 'FP', 'TN', 'FN']) <= set(c.keys())
    TP = numpy.float64(c['TP'])
    TN = numpy.float64(c['TN'])
    FP = numpy.float64(c['FP'])
    FN = numpy.float64(c['FN'])
    output = {
        'TP': TP,
        'FP': FP,
        'TN': TN,
        'FN': FN,
        'TPR': TP / (TP + FN),
        'TNR': TN / (TN + FP),
        'PPV': TP / (TP + FP),
        'NPV': TN / (TN + FN),
        'FNR': FN / (FN + TP),
        'FPR': FP / (FP + TN),
        'FDR': FP / (FP + TP),
        'FOR': FN / (FN + TN),
        'ACC': (TP + TN) / (TP + TN + FP + FN),
        'F1':  2*TP / (2*TP + FP + FN),
        'MCC': (TP*TN - FP*FN) / ((TP+FP) * (TP+FN) * (TN+FP) * (TN+FN))
    }
    output['BM'] = output['TPR'] + output['TNR'] - 1
    output['MK'] = output['PPV'] + output['NPV'] - 1
    return output
# end def


# function to transfer data from one histogram to another with different bins
def histogram_change_base(H, xs_new):
    """Change the base of a histogram

    Args:
        :H (Series_x, Series_y): Input histogram. must be a two series items of equal length
        :xs_new (Series): new base of histogram. must be a series of Float

    Returns:
        :Histogram (Series_x, Series_y)
    """

    import scipy.interpolate
    xs, ys = H

    # interpolate
    f = scipy.interpolate.interp1d(xs, ys, kind='cubic')
    # filter oulier
    xs = [_ for _ in xs_new if min(xs) <= _ <= max(xs)]
    # genrate output
    ys = f(xs)

    return xs, ys
# end def


class discrete_histogram():
    def __init__(self, bins):
        self.bins = numpy.array(list(sorted(bins)))
        self.lb, self.ub = self.bins[0], self.bins[-1]
        self.mids = 0.5*(self.bins[:-1] + self.bins[1:])

        self.counts = numpy.zeros(len(self.bins)-1, dtype=int)
        self.lb_count = 0
        self.ub_count = 0
    # end def

    def feed(self, data):
        for dat in data:
            if dat < self.lb:
                self.lb_count += 1
                continue
            if dat > self.ub:
                self.ub_count += 1
                continue
            for i in range(len(self.bins)-1):
                if self.bins[i] <= dat < self.bins[i+1]:
                    self.counts[i] += 1
                # end if
            # end for
        # end for
    # end def

    def get_summary(self):
        n_total = sum(self.counts) + self.lb_count + self.ub_count
        _summary = {
            'N': n_total,
            'N>UB': self.ub_count,
            'N<LB': self.lb_count,
            'mean': self.calculate_mean(),
            'stdev': self.calculate_stdev()
        }
        return _summary
    # end def

    def normalize(self):
        self.counts = self.counts / sum(self.counts)
    # end def

    def calculate_mean(self):
        counts = numpy.array(self.counts)
        return sum(counts * self.mids) / sum(counts)
    # end def

    def calculate_stdev(self):
        u = self.calculate_mean()
        return numpy.sqrt(sum(self.counts * (self.mids - u)**2) / sum(self.counts))
    # end def
# end class


def chi2_one_vs_all(vals):
    """Performs chi-squared tests for each row in the table vs all other rows

    Args:
        :vals (array): Input frequency table

    Returns:
        :List: List of dictionary with keys {'chi2', 'p', 'dof', 'expected'} from the chi-squared tests
    """

    vsum = vals.sum(axis=0)
    output = []
    for i in range(len(vals)):
        row = vals[i]
        tbl = numpy.array([vsum - row, row])
        if row.min() < 5:
            logger.warning('row %d has a value < 5' % (i+1, ))
        # end if
        result = scipy.stats.chi2_contingency(tbl)
        result = dict(zip(['chi2', 'p', 'dof', 'expected'], result))
        output.append(result)
    # end for
    return output
# end def

def compute_mic(x, y):
	'''Compute maximal information coefficient for a variable pair
	Reference: https://arxiv.org/pdf/1301.7745v1.pdf
	'''
	mine = MINE(est="mic_approx")
	mine.compute_score(x, y)
	result = {
	  "MIC": mine.mic(),
	  "MAS": mine.mas(),
	  "MEV": mine.mev(),
	  "MCN (eps=0)": mine.mcn(0),
	  "MCN (eps=1-MIC)": mine.mcn_general(),
	  "GMIC": mine.gmic(),
	  "TIC": mine.tic(),
	} # end resutl
	return {'mic': mine.mic(), 'result': result}
# end def

