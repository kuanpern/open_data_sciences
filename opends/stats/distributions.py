import copy
import warnings
import random
import numpy as np
import scipy
import scipy.stats
import scipy.spatial
import logging
logger = logging.getLogger(__name__)

__distributions = ['beta', 'cauchy', 'chi2', 'cosine', 'expon', 'f', 'gamma', 'gompertz', 'gumbel_l', 'gumbel_r', 
                'halfcauchy', 'halflogistic', 'halfnorm', 'halfnorm', 'invgamma', 'laplace', 'levy', 'levy_l', 
                'loggamma', 'logistic', 'loglaplace', 'lognorm', 'loguniform', 'norm', 
                'pareto', 'powerlaw', 'rayleigh', 'skewnorm', 't', 'triang', 'uniform', 'vonmises', 'wald', 
                'weibull_max', 'weibull_min']

def fit_distributions(s, candidates=__distributions):
    # TODO: type checkiing
    vals = copy.copy(s)
    random.shuffle(copy.copy(vals))

    # split into training and testing sets. only 1/2 splitting support now
    n = int(len(vals) / 2.)
    train, test = vals[:n], vals[n:]
    
    def compute_ks_dist(train, test, cand_dist, pts='auto'):
        # TODO: type checkiing
        '''Kolmogorov-Smirnov test'''
        param = cand_dist.fit(train)
        dist  = cand_dist(*param[:-2], loc=param[-2], scale=param[-1])

        if pts == 'auto':
            pts = np.linspace(vals.min(), vals.max(), 100)
        # end if
        
        cdf_train = dist.cdf(pts)
        cdf_test  = np.array([scipy.stats.percentileofscore(test, pt) for pt in pts]) / 100

        D = scipy.spatial.distance.minkowski(cdf_train, cdf_test, p=float('inf'))
        return D
    # end def
    
    def func(distname):
        logger.info('testing on %s ..' % (distname,))
        cand_dist = getattr(scipy.stats, distname)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            out = compute_ks_dist(train, test, cand_dist)
        # end with
        return out
    # end def
    
    return dict(zip(__distributions, map(func, __distributions)))
# end def
