"""
Machine Learning Shorthands
"""


import copy
import random
import numpy
import pandas
from . import shorthand
#import shorthand

# ========= MACHINE LEARNING ========= #
# construct crossvalidation dataset while maintaining the composition of True and False
# note: this is the fold-preserving version of sklearn.cross_validation.train_test_split
def construct_CV_datasets(DF, label_name, n_cv=10, random_seed=None):
    """Construct crossvalidation dataset while maintaining the composition of True and False

    Args:
        :DF: input pandas dataframe
        :label_name (string): column name for binary label
        :n_cv: number of cross-validation data sets
        :random_seed: random seed

    Returns:
        :List: [{'training': dataframe, 'testing': dataframe}, ...]
    """
    # this is binary True/False only .... need to improve

    # separate into traininging and testinging set
    boxes = [{'training': [], 'testing': []} for _ in range(n_cv)]

    # separate the true and false cases
    ys_t = list(DF[DF[label_name] ==  True].index)
    ys_f = list(DF[DF[label_name] == False].index)
    DF_T = DF.loc[ys_t]
    DF_F = DF.loc[ys_f]

    # filling the positive cases into boxes
    for i in range(len(DF_T)):
        skip_index = i % n_cv
        for box_index in range(n_cv):
            if box_index == skip_index:
                boxes[box_index]['testing'] .append(DF_T.iloc[i])
            else:
                boxes[box_index]['training'].append(DF_T.iloc[i])
            # end if
        # end for
    # end for

    # filling the negative cases into boxes
    for i in range(len(DF_F)):
        skip_index = i % n_cv
        for box_index in range(n_cv):
            if box_index == skip_index:
                boxes[box_index]['testing'] .append(DF_F.iloc[i])
            else:
                boxes[box_index]['training'].append(DF_F.iloc[i])
            # end if
        # end for
    # end for

    # make into dataframe
    for i in range(len(boxes)):
        boxes[i]['training'] = pandas.DataFrame(shorthand.shuffle(boxes[i]['training'], random_seed=random_seed))
        boxes[i]['testing']  = pandas.DataFrame(shorthand.shuffle(boxes[i]['testing' ], random_seed=random_seed))
    # end for

    return boxes
# end def

# sample such that (i) the smaller set is always present (ii) the bigger set is randomly sampled that the number of case is same with smaller set (iii) bigger set is sampled multiple time to ensure chance of picking every case at least one is at least q. Assume binary class of True and False
# note: this is the fold-preserving version of sklearn.cross_validation.train_test_split
def sample_balance_datasets(DF, label_name, q=0.95, random_seed=None):
    """Sample balanced data set from a dataframe by a binary class label

    Args:
        :DF: input pandas dataframe
        :label_name (string): column name for binary label
        :q (float): minimum probability one item will be selected at least once
        :random_seed: random seed

    Returns:
        :List of pandas dataframes
    """
    if random_seed is not None:
        random.seed(random_seed)
    # end if

    labels = list(DF[label_name].values)
    assert set(labels) == {True, False}

    # sample at least n_sample time so that chance of picking every case at least one is at least q
    c = shorthand.count_elements(labels)
    # nothing to do
    if c[True] == c[False]:
        return DF
    # end if

    # find out which is the small set
    if c[True] < c[False]:
        sel = True
    else:
        sel = False
    # end if
    # number of times to sample the largeger set
    n_sample = int(numpy.log(1-q)/numpy.log(1-c[sel]/c[not(sel)])+1)

    # separate into sel and not-sel set (sel=which has the lower number (e.g. True))
    dets = labels == sel

    # small set and large set
    z_small = DF[DF[label_name] == sel]
    z_large = DF[DF[label_name] != sel]

    # sampling the large
    output = []
    indexes = copy.deepcopy(list(z_large.index))
    for _iter in range(n_sample):
        random.shuffle(indexes)
        df_sel = z_large.loc[indexes][:len(z_small)]

        df_out = z_small.append(df_sel)
        output.append(df_out)
    # end for

    # shuffle a bit ...
    temp = []
    for df_out in output:
        indexes = copy.deepcopy(list(df_out.index))
        random.shuffle(indexes)
        df_shuf = df_out.loc[indexes]

        temp.append(df_shuf)
    # end for
    output = temp

    return output
# end def
