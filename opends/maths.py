"""
Additional Mathematical Functions
"""

import numpy
import scipy
from . import stats
from . import shorthand
#import stats
#import shorthand


def add_stdevs(std_a, std_b, corrcoef=0):
    """adding two standard deviations

    Args:
        std_a (float): The first standard deviation
        std_b (float): The second standard deviation
        corrcoef (float): The correlation coefficient between the two data sets

    Returns:
        Float: summed standard deviation
    """
    var_1, var_2 = std_a**2, std_b**2
    var_sum = var_1 + var_2 + 2*(std_a*std_b*corrcoef)
    return numpy.real(numpy.sqrt(var_sum))
# end def


def substract_stdevs(std_a, std_b, corrcoef=0):
    """substract standard deviations from another

    Args:
        std_a (float): The first standard deviation
        std_b (float): The second standard deviation
        corrcoef (float): The correlation coefficient between the two data sets

    Returns:
        Float: substracted standard deviation
    """
    var_1, var_2 = std_a**2, std_b**2
    var_sum = var_1 + var_2 - 2*(std_a*std_b*corrcoef)
    return numpy.real(numpy.sqrt(var_sum))
# end def


def cov2corr(v):
    """convert covariance matrix to correlation matrix

    Args:
        v: input covariance matrix

    Returns:
        correlation matrix
    """
    dim = len(v)
    assert(numpy.shape(v)) == (dim, dim)
    D = numpy.sqrt(numpy.eye(dim)*numpy.diag(v))
    Dinv = numpy.linalg.inv(D)
    r = numpy.asmatrix(Dinv) * numpy.asmatrix(v) * numpy.asmatrix(Dinv)
    return r
# end def


def gaussian_coef(n, a=0.95):
    """return a series of normalized Gaussian coefficients

    Args:
        n (int): number of terms
        a (float): minimum value of sum of series

    Returns:
        Normalized Gaussian coefficients
    """

    def choose_center(data, n):
        """choose the center n term from a list
        """
        start = int((len(data) - n) / 2.0)
        return data[start:start+n]
    # end def

    def pascal_triangle(n):
        """return a series of Pascal triangle number
        """
        assert type(n) == int
        if n == 1:
            return [1]
        # end if

        old_series = [1]
        for i in range(n+1):
            new_series = [1]
            for j in range(1, i):
                new_series.append(old_series[j-1] + old_series[j])
            # end for
            new_series.append(1)
            old_series = new_series
        # end for

        return old_series
    # end def

    k = n - 1
    while True:
        k = k + 1
        trial = choose_center(shorthand.normalize(pascal_triangle(k-1)), n)
        if sum(trial) > a:
            return trial
        # end if
    # end while
# end def


def exponential_coef(n, ratio):
    """Return a series of exponentially decreasing coefficients (geometrical series)

    Args:
        n (int): number of terms
        ratio (float): ratio of the series. must be (0, 1)

    Returns:
        Exponential series coefficients
    """

    assert 0 < ratio < 1, 'ratio must be (0, 1)'

    output = [1.0]
    for i in range(n):
        output.append(output[-1]*ratio)
    # end for
    return output
# end def


def eig_decompose(A):
    """Eigen-decomposition of matrix A = PDP^-1

    Args:
        A: input matrixnumber of terms

    Returns:
        v: matrix
        B: diagnonal matrix
        vi: inverse of v
    """

    x, y = numpy.shape(A)
    assert x == y, 'A is not square'

    la, v = numpy.linalg.eig(A)
    v = numpy.asmatrix(v)
    B = numpy.zeros([len(la), len(la)])
    for i in range(len(la)):
        B[i][i] = la[i]
    # end for
    vi = numpy.linalg.inv(v)
    return (v, B, vi)
# end def


# discretize a numerical scale into nominal scale. each nominal scale contains approximately equal number of elements.
def equal_size_binning(s, n=5, labels=None):
    """Equally categorize into classes and label a series by the value ascendingly.

    Args:
        s (list, tuple): number of terms
        n (int): number of classes
        labels: labels to the classes. If None, use range(n) by default

    Returns:
        Series of class labels
    """

    if labels is None:
        labels = list(range(n))
    # end if

    cuts = numpy.linspace(0, 100, n+1, endpoint=True)
    thresholds = numpy.percentile(s, cuts)[1:-1]

    cls = shorthand.ThresholdNameClassifier(names=labels, thresholds=thresholds)
    labels = cls.parse(s)
    
    return labels
# end def


def filter_outliers(S, sig_thr=2.5, max_iter=10):
    """Filter outlier by iteratively remove points x std. dev away from mean

    Args:
        S: Input series
        sig_thr: number of standard deviation to filter points
        max_iter (int): maximum number of iteration

    Returns:
        retained (List): inliers
        outliers (List): outliers
        ranges: min(retained), max(retained)
    """

    outliers = []
    iteration = 0
    while True:
        iteration += 1

        # majority consensus ...
        u, iqr = [numpy.median(S), stats.IQR(S)]
        sigma = iqr / 1.35
        thr = sig_thr * sigma
        lb, ub = [u - thr, u + thr]

        # segregate inliers and outliers
        retained, new_outliers = [[], []]
        for dat in S:
            if lb <= dat <= ub:
                retained.append(dat)
            else:
                new_outliers.append(dat)
            # end if
        # end for
        if len(new_outliers) == 0:
            return retained, outliers, [min(retained), max(retained)]
        else:
            S = retained
            outliers.extend(new_outliers)
        # end if

        if iteration >= max_iter:
            break
        # end if
    # end while

    return retained, outliers, [min(retained), max(retained)]
# end def
