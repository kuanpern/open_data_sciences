"""
Common Utilties for Database / Datalake Connection
"""

from . import shorthand
#import shorthand


@shorthand.require_inputs(['username', 'password', 'host', 'database'])
def gen_mysql_url(pars):
	"""Generate MySQL server connection string. Only supports mysqldb bridge.

	Args:
		:Dict: keys should include {'username', 'password', 'host', 'port', 'database_name'}

	Returns:
		:String: mysql connection string
	"""

	if 'port' not in pars:
		pars.update({'port': 3306})
	# end if

	# initialize a mysql session
	mysql_url = 'mysql+mysqldb://{username}:{password}@{host}:{port}/{database}'.format(**pars)

	return mysql_url
# end def


@shorthand.require_inputs(['username', 'password', 'host', 'database'])
def connect_mysql_server(pars):
	"""Establish a connection to a mysql database. Only supports sqlalchemy bridge.

	Args:
		:Dict: keys should include {'username', 'password', 'host', 'database'}

	Returns:
		:Tuple: (engine, connection)
	"""

	conn_str = gen_mysql_url(pars)
	return connect_sql_server(conn_str)
# end def


def connect_sql_server(conn_str):
	import sqlalchemy
	engine = sqlalchemy.create_engine(conn_str)
	connection = engine.connect()
	return (engine, connection)
# end def


@shorthand.require_inputs(['username', 'password', 'server_ip', 'server_port', 'database_name'])
def connect_cassandra_server(pars):
	"""Establish a connection to a Cassandra database

	Args:
		:Dict: keys should include {'username', 'password', 'server_ip', 'server_port', 'database_name'}

	Returns:
		:Object: Cassandra DB connection session
	"""
	import cassandra

	username  = pars['username']
	password  = pars['password']
	server_ip = pars['server_ip']
	port	  = pars['server_port']
	database  = pars['database_name']

	# connect to server and start a session
	auth_provider = cassandra.auth.PlainTextAuthProvider(
		username=username,
		password=password,
	)
	cluster = cassandra.cluster.Cluster([server_ip], port=port, auth_provider=auth_provider)
	session = cluster.connect()
	session.execute('USE ' + database + ';')

	return session
# end def


@shorthand.require_inputs(['username', 'password', 'cluster_url', 'database'])
def gen_mongodb_url(pars):
	"""Generate MongoDB cluster connection string

	Args:
		:Dict: keys should include {'username', 'password', 'cluster_url', 'database_name'}, optionally {'authSource', 'replicaSetName', 'readPreference'}

	Returns:
		:String: MongoDB connection string
	"""

	# initialize a mongoDB session
	mongodb_url = 'mongodb://{username}:{password}@{cluster_url}/{database}'.format(**pars)

	# add any optional arguments
	opt_args = []
	if 'authSource' in list(pars.keys()):
		opt_args.append('authSource='+pars['authSource'])
	if 'replicaSetName' in list(pars.keys()):
		opt_args.append('replicaSet='+pars['replicaSetName'])
	if 'readPreference' in list(pars.keys()):
		opt_args.append('readPreference='+pars['readPreference'])

	if len(opt_args) != 0:
		opt_args = '&'.join(opt_args)
		mongodb_url = mongodb_url + '?' + opt_args
	# end if

	return mongodb_url
# end def


@shorthand.require_inputs(['username', 'password', 'cluster_url', 'database'])
def connect_mongoDB_server(pars):
	"""Establish a connection to a MongoDB database

	Args:
		:Dict: keys should include {'username', 'password', 'cluster_url', 'database'}

	Returns:
		:Tuple: (DB object, client connection)
	"""
	import pymongo

	mongodb_url = gen_mongodb_url(pars)
	client = pymongo.MongoClient(mongodb_url)
	db = client[pars['database']]

	# return client, so that it can be closed from app
	return (db, client)
# end def


@shorthand.require_inputs(['user', 'host', 'port'])
def connect_hdfs_server(pars):
	# TODO: security not implemented yet
	"""Establish a connection to HDFS server

	Args:
		:Dict: keys should include {'user', 'host', 'port'}

	Returns:
		:Object: HDFS client connection
	"""

	import hdfs
	user, host, port = list(map(pars.get, ['user', 'host', 'port']))
	server_url = 'http://{host}:{port}'.format(host=host, port=port)

	# open client connection
	client = hdfs.InsecureClient(server_url, user=user)
	return client
# end def
