import os
import slack
import requests

def send_sms_push_bullet(recipient_no, sender_name, message, device_id, api_key):

	endpoint = 'https://api.pushbullet.com/v2/ephemerals'
	headers = {
		'Access-Token': api_key,
		'Content-Type': 'application/json',
	}

	data = {
	  "push": {
		"conversation_iden": recipient_no,
		"source_user_iden": sender_name,
		"message": message,
		"target_device_iden": device_id,
		"package_name": "com.pushbullet.android",
		"type": "messaging_extension_reply"
	  },
	  "type": "push"
	}

	# actually send
	response = requests.post(endpoint, headers=headers, json=data)
	return response
# end def
# alias
send_sms = send_sms_push_bullet

# Sending a slack message
# note: only basic send message function defined here. for more, see https://pypi.org/project/slackclient/
def send_slack_message(channel, text, token, from_user='bot', to_user=None):
	# initialize connection
	client = slack.WebClient(token=token)

	# send the message
	if to_user is None:
		response = client.chat_postMessage(channel=channel, text=text, username=from_user)
	else:
		response = client.chat_postMessage(channel=channel, text=text, username=from_user, to_user=to_user)
	# end if
	assert response["ok"]
# end def
