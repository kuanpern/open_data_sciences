import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
# end with

setuptools.setup(
    name="opends",
    version="0.2.1",
    author="Tan Kuan Pern",
    author_email="kptan86@gmail.com",
    description="Collection of data sciences capability and python snippets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://kuanpern@bitbucket.org/kuanpern/open_data_sciences/src/master/",
    packages=setuptools.find_packages(),
    install_requires=[
      'pycrypto>=2',
      'Jinja2>=2',
      'matplotlib>=3',
      'pandas>=0.20',
      'requests>=2',
      'scipy>=1',
      'sendgrid==6.1.0',
      'SQLAlchemy>=1',
      'PyYAML>=3',
      'postmarker>=0.12',
      'slackclient>=2.1.0',
      'minepy>=1.1',
      'anytree>=2.5.0',
      'openpyxl>=3.0',
      'eralchemy>=1.0',
      'pygraphviz>=1.4',
      'pygit2>=1.1',
      'git-deps>=1.0',
      'jupyter>=1.0',
    ]
)
