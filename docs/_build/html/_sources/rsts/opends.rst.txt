opends package
==============

Submodules
----------

opends.ML\_shorthand module
---------------------------

.. automodule:: opends.ML_shorthand
    :members:
    :undoc-members:
    :show-inheritance:

opends.common\_utils module
---------------------------

.. automodule:: opends.common_utils
    :members:
    :undoc-members:
    :show-inheritance:

opends.conf module
------------------

.. automodule:: opends.conf
    :members:
    :undoc-members:
    :show-inheritance:

opends.easy\_messaging module
-----------------------------

.. automodule:: opends.easy_messaging
    :members:
    :undoc-members:
    :show-inheritance:

opends.easymail module
----------------------

.. automodule:: opends.easymail
    :members:
    :undoc-members:
    :show-inheritance:

opends.maths module
-------------------

.. automodule:: opends.maths
    :members:
    :undoc-members:
    :show-inheritance:

opends.shorthand module
-----------------------

.. automodule:: opends.shorthand
    :members:
    :undoc-members:
    :show-inheritance:

opends.stats module
-------------------

.. automodule:: opends.stats
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opends
    :members:
    :undoc-members:
    :show-inheritance:
