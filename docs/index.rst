Welcome to open_ds's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rsts/modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
