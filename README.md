## OPEN DATA SCIENCES
Collection of data sciences capability and python snippets

Note: The package comes with ABSOLUTELY NO WARRANTY. Use at your own risk.

### INSTALL

#### PREREQUISITES
```
$ sudo apt-get install g++ python3-pygit2
```


#### INSTALL WITH PIP
```
pip install -e git+https://bitbucket.org/kuanpern/open_data_sciences/open_data_sciences.git#egg=opends
```
