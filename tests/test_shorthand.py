"""
LGLP LICENSE
Copyright (c) 2016 Tan Kuan Pern
"""

import datetime
import random
import dateutil.parser
import numpy as np
import os
import copy
import opends.shorthand

def test_next_day():
    today = datetime.datetime.now().isoformat().split('T')[0]
    test_days = [0, 1, 7, 30, 366, 3700]
    for t in test_days:
        day_diff = dateutil.parser.parse(opends.shorthand.next_day(t)) - dateutil.parser.parse(today)
        assert day_diff.days == t
    # end for
# end def

# IO, formats
def test_yesterday():
    today = datetime.datetime.now().isoformat().split('T')[0]
    day_diff = dateutil.parser.parse(opends.shorthand.yesterday()) - dateutil.parser.parse(today)
    assert day_diff.days == -1
# end def


def test_today():
    today = datetime.datetime.now().isoformat().split('T')[0]
    day_diff = dateutil.parser.parse(opends.shorthand.today()) - dateutil.parser.parse(today)
    assert day_diff.days == 0
# end def


def test_tomorrow():
    today = datetime.datetime.now().isoformat().split('T')[0]
    day_diff = dateutil.parser.parse(opends.shorthand.tomorrow()) - dateutil.parser.parse(today)
    assert day_diff.days == 1
# end def


def test_subdivide_data():
    N = random.randint(500, 1000)
    n = random.randint(3, 100)

    inputs = range(N)
    outputs = opends.shorthand.subdivide_data(inputs, n)

    temp = []
    [temp.extend(_) for _ in outputs];

    assert sorted(temp) == sorted(inputs)
    r = int(N / n)
    assert len(outputs) in [r, r+1]
# end def


def test_getattrs():
    r = random.random()
    class test_obj_cls:
        def __init__(self):
            self.x = r
        # end def
    # end def
    test_obj = test_obj_cls()
    assert opends.shorthand.getattrs(test_obj)('x') == r
# end def


def test_swap():
    p = round(random.random(), 2)
    q = p * 2
    orig = copy.deepcopy((p, q))
    ans = opends.shorthand.swap(p, q)
    assert ans == orig
# end def


# MATH #
def test_min_max_index():
    s = [random.random() for i in range(100)]
    s = list(set(s))
    [min_value, min_index], [max_value, max_index] = opends.shorthand.min_max_index(s)
    assert s[min_index] <= min(s)
    assert s[max_index] >= max(s)
# end def


def test_min_index():
    s = [random.random() for i in range(100)]
    s = list(set(s))
    [min_value, min_index], [max_value, max_index] = opends.shorthand.min_max_index(s)
    assert s[min_index] <= min(s)
# end def


def test_max_index():
    s = [random.random() for i in range(100)]
    s = list(set(s))
    [min_value, min_index], [max_value, max_index] = opends.shorthand.min_max_index(s)
    assert s[max_index] >= max(s)
# end def


def test_weighted_average():
    data    = [-26.0, -11.3, -4.9, -2.7, -0.6, 5.2, 19.0, 44.5, 86.0]
    weights = [1, 4, 2, 4, 2, 4, 2, 4, 1]
    ans = opends.shorthand.weighted_average(data, weights)
    assert ans == 9.575
# end def


def test_normalize():
    # generate random data
    x = [random.random()*20 for i in range(100)]

    # get the ranking of data
    def get_ranking(x):
        y = [[x[i], i] for i in range(len(x))]
        y.sort()
        z = list(zip(*y))[1]
        w = [[z[i], i] for i in range(len(z))]
        w.sort()
        ranking = list(zip(*w))[1]
        return ranking
    # end def

    normed = opends.shorthand.normalize(x)
    assert get_ranking(normed) == get_ranking(x)
    assert abs(sum(normed) - 1) < 0.00001
# end def


def test_normalize_dict():
    # generate random data
    keys = list('qwertyuiopasdfghjklzxcvbnm')
    vals = [random.random() for _ in keys]
    data = dict(zip(keys, vals))

    # get the ranking of data
    def get_ranking(x):
        y = [[x[i], i] for i in range(len(x))]
        y.sort()
        z = list(zip(*y))[1]
        w = [[z[i], i] for i in range(len(z))]
        w.sort()
        ranking = list(zip(*w))[1]
        return ranking
    # end def

    normed = opends.shorthand.normalize_dict(data)
    normed_keys = normed.keys()
    normed_vals = normed.values()
    vals = [data[key] for key in normed_keys]

    assert abs(sum(normed_vals) - 1) < 0.00001
    assert opends.shorthand.get_ranking(vals) == opends.shorthand.get_ranking(normed_vals)

# end def


def test_isint():
    assert opends.shorthand.isint(5.0) is True
    assert opends.shorthand.isint(np.inf) is False
    assert opends.shorthand.isint(-np.inf) is False
    assert opends.shorthand.isint(5.3) is False
# end def


# FILE IO #
def test_get_file_timestamp():
    import os
    import uuid

    time1 = datetime.datetime.now()
    outfilename = '/tmp/'+str(uuid.uuid4())+'.temp'
    with open(outfilename, 'w') as fout:
        fout.writelines('')
    # end with
    time2 = dateutil.parser.parse(opends.shorthand.get_file_timestamp(outfilename))
    os.remove(outfilename)

    # note: assume the two operation will be within 1 minute 
    assert (time2-time1).total_seconds() < 60
# end def


def test_shuffle():
    # note: randomness is not tested
    import copy
    s = [random.random() for _ in range(100)]
    t = opends.shorthand.shuffle(copy.deepcopy(s))

    assert list(sorted(s)) == list(sorted(t))
# end def


def test_remove_from_list():
    import copy
    s = list('qwertyuiioopasdfghjkklzxcvbnm')
    w = copy.deepcopy(s)
    random.shuffle(w)
    n = random.choice([1, 2, 3, 4])
    unwanteds = w[:n]

    ans = opends.shorthand.remove_from_list(s, unwanteds)
    assert len(set(ans) & set(unwanteds)) == 0
    assert set(list(ans) + list(unwanteds)) == set(s)
# end def


def test_files_in_directory():
    import uuid
    import os

    while True:
        directory = '/tmp/'+str(uuid.uuid4())
        if not os.path.exists(directory):
            os.makedirs(directory)
            break
        # end if
    # end while

    # positive test
    filenames_p = [str(uuid.uuid4())+'.temp' for i in range(3)]
    filenames_p = list(set(filenames_p))
    for filename in filenames_p:
        with open(directory+os.sep+filename, 'w') as fout:
            fout.writelines('')
        # end with
    # end for
    filenames_q = ['prefix-'+str(uuid.uuid4()) for i in range(3)]
    filenames_q = list(set(filenames_q))
    for filename in filenames_q:
        with open(directory+os.sep+filename, 'w') as fout:
            fout.writelines('')
        # end with
    # end for

    files_p = opends.shorthand.files_in_directory(directory, suffixes=['temp'])
    files_q = opends.shorthand.files_in_directory(directory, prefixes=['prefix'])
    files_r = opends.shorthand.files_in_directory(directory, prefixes=['prefix'], suffixes=['temp'])

    filenames_p = [directory+os.sep+filename for filename in filenames_p]
    filenames_q = [directory+os.sep+filename for filename in filenames_q]

    assert set(files_p) == set(filenames_p)
    assert set(files_q) == set(filenames_q)
    assert len(set(files_r)) == 0
# end def


def test_filter_printable():
    assert opends.shorthand.filter_printable('•f•Åoo') == 'foo'
# end def


def test_compact_text():
    s = 'foo  bar spam     eggs  '
    ans = opends.shorthand.compact_text(s)
    assert ans == 'foo bar spam eggs'
# end def

def test_timestamp():
    ans = opends.shorthand.timestamp()
    _diff = datetime.datetime.now() - dateutil.parser.parse(ans)
    # assume two operations within 30 seconds
    assert _diff.total_seconds() < 30
# end def


def test_utc_timestamp():
    ans = opends.shorthand.utc_timestamp()
    epoch = datetime.datetime(1970, 1, 1)
    s = (datetime.datetime.utcnow() - epoch).total_seconds()
    # assume two operations within 30 seconds
    assert ans - s < 30
# end def


def test_mkdir_p():
    import uuid
    dirname = '/tmp/'+str(uuid.uuid4())

    # positive case
    try:
        os.removedirs(dirname)
    except FileNotFoundError:
        pass
    # end try
    opends.shorthand.mkdir_p(dirname)

    assert os.path.isdir(dirname)
    os.removedirs(dirname)
# end def


def test_create_dir_if_necessary():
    import uuid
    dirname = '/tmp/'+str(uuid.uuid4())

    # positive case
    try:
        os.removedirs(dirname)
    except FileNotFoundError:
        pass
    # end try
    opends.shorthand.create_dir_if_necessary(dirname)

    assert os.path.isdir(dirname)
    os.removedirs(dirname)
# end def


def test_tail():
    import os
    import uuid
    while True:
        filename = '/tmp/'+str(uuid.uuid4())+'.temp'
        if os.path.isfile(filename):
            continue
        else:
            inlines = [str(uuid.uuid4()) for _ in range(20)]
            content = '\n'.join(inlines) + '\n'
            with open(filename, 'w') as fout:
                fout.writelines(content)
            # end with
            break
        # end if
    # end while
            
    n_line = 10
    lines = opends.shorthand.tail(filename, n_line)
    assert lines == inlines[-n_line:]

    os.remove(filename)
# end def


# standard exception catching decorator
def test_std_exc_catch():
    @opends.shorthand.std_exc_catch
    def test_func():
        raise ValueError('no value')
    # end def
    ans = test_func()
    assert ans == {'data': [], 'errors': ["ValueError('no value',)"], 'status': 'failed'}
# end def


def test_sort_by_keys():
    # TODO
    pass
# end def


def test_count_elements():
    alphas = list('qwertyuiopasdfghhjklzxcvbnm')
    counts = {alpha: random.randrange(1, 7) for alpha in alphas}
    s = []
    for alpha, val in counts.items():
        s.extend([alpha]*val)
    # end for
    random.shuffle(s)

    ans = opends.shorthand.count_elements(s)
    assert ans == counts
# end def


def test_choose_majority():
    alphas = list('qwertyuiopasdfghhjklzxcvbnm')
    counts = {alpha: random.randrange(1, 7) for alpha in alphas}
    s = []
    for alpha, val in counts.items():
        s.extend([alpha]*val)
    # end for
    random.shuffle(s)

    ans = opends.shorthand.choose_majority(s)
    counts[ans] >= max(counts.values())
# end def


def test_isnan():
    import numpy
    assert opends.shorthand.isnan(numpy.nan) is True
    assert opends.shorthand.isnan('foobbar') is False
# end def


def test_require_inputs():

    @opends.shorthand.require_inputs(["username", "password", "conn_string"])
    def test_db_conn(input_pars):
        username = input_pars['username']
        password = input_pars['password']
        conn_str = input_pars['conn_string']
    # end def

    # negative test
    ans = test_db_conn({'username': 'foo', 'conn_string': 'bar'})
    assert ans['status'] == 'failed'

    # positive test
    ans = test_db_conn({'username': 'foo', 'password': 'eggs', 'conn_string': 'bar'})
    assert ans is None
# end def


# class method version
def test_cls_require_inputs():

    class Ham:
        @opends.shorthand.cls_require_inputs(["username", "password", "conn_string"])
        def test_db_conn(self, input_pars):
            username = input_pars['username']
            password = input_pars['password']
            conn_str = input_pars['conn_string']
            return
        # end def
    # end class

    ham = Ham()
    ans = ham.test_db_conn({'username': 'foo', 'conn_string': 'bar'})
    assert ans['status'] == 'failed'
    ans = ham.test_db_conn({'username': 'foo', 'password': 'eggs', 'conn_string': 'bar'})
    assert ans is None
# end def


def test_num2rank():
    # generate a sequence with known ranking
    t = list(range(10))
    random.shuffle(t)
    s = [None for _ in t]
    val = 0
    for i in range(10):
        j = t.index(i)
        s[j] = val
        val += random.random()
    # end for

    ans = list(opends.shorthand.num2rank(s))
    assert ans == t
# end def


def test_meta_parse_args():
    # TODO - how to test ?
    pass
# end def


def test_import_module():
    # TODO - how to test ?
    pass
# end def

def test_ConfigurationHandler():
    # TODO
    pass
# end class


def test_ThresholdNameClassifier():
    thresholds = [-2, -1, 0, 1, 2]
    names = ['neg_outlier', 'neg_normal', 'neg', 'pos', 'pos_normal', 'pos_outlier']
    cls = opends.shorthand.ThresholdNameClassifier(thresholds=thresholds, names=names)

    input_values = [-2.6, -3.0, 4.4, 0.2, 0.2, 2.3, 1.4, 2.6, -1.6, -3.9]
    labels = cls.parse(input_values)
    assert labels == ['neg_outlier', 'neg_outlier', 'pos_outlier', 'pos', 'pos', 
                'pos_outlier', 'pos_normal', 'pos_outlier', 'neg_normal', 'neg_outlier']
# end def

def test_RetryException():
    exp = 'something went wrong'
    max_retry = 5
    err = opends.shorthand.RetryException(exp, max_retry)
    assert err.exp == exp
    assert err.max_retry == max_retry
# end def


def test_retry_func():
    err = None
    msg = 'something went wrong'
    try:
        @opends.shorthand.retry_func(4)
        def gen_error(msg):
            raise ValueError(msg)
        # end def
        gen_error(msg)
    except Exception as e:
        err = e
    # end try
    assert isinstance(err, opends.shorthand.RetryException)
    assert err.exp.args[0] == msg
# end def


def test_EmptyObj():
    obj = opends.shorthand.EmptyObj({'x': 1, 'y': 2})
    assert obj.x == 1
    assert obj.y == 2
# end def


# legacy class name for emptyObj
def test_Struct():
    obj = opends.shorthand.Struct({'x': 1, 'y': 2})
    assert obj.x == 1
    assert obj.y == 2
# end def


# taken from https://stackoverflow.com/questions/7406102/create-sane-safe-filename-from-any-unsafe-string
def test_gen_safe_filename():
    input_filename = '/tmp/spam /foo ^$ba(r.txt'
    ans = opends.shorthand.gen_safe_filename(input_filename)
    assert ans == '/tmp/spam/foobar.txt'
# end def


# taken from http://matthiaseisen.com/pp/patterns/p0198/
def test_jinja_render():
    # TODO: how to test ?
    pass
# end def


def test_this_file():
    # TODO: how to test ?
    pass
# end def


def test_SimpleEncryptor():
    alphanum = '1234567890qwertyuiopasdfghjklzxcvbnm ,.; '
    secret_key = ''.join([alphanum[random.randrange(0, len(alphanum))] for _ in range(random.randrange(6, 12))])
    msg = ''.join([alphanum[random.randrange(0, len(alphanum))] for _ in range(random.randrange(20, 100))])

    encrytor = opends.shorthand.SimpleEncryptor()
    encrypted = encrytor.simple_encrypt(msg, secret_key)
    decrypted = encrytor.simple_decrypt(encrypted, secret_key)
    assert decrypted == msg
# end def


def test_decode_base64():
    import base64

    alphanum = '1234567890qwertyuiopasdfghjklzxcvbnm ,.; '
    msg = ''
    for _ in range(random.randrange(20, 100)):
        index = random.randrange(0, len(alphanum))
        msg += alphanum[index]
    # end for
    msg = str.encode(msg)

    encoded = base64.b64encode(msg)
    decoded = opends.shorthand.decode_base64(encoded)
    assert decoded == msg
# end def


def test_split_sublist():
    N = random.randint(500, 1000)
    n = random.randint(3, 100)

    inputs = range(N)
    outputs = opends.shorthand.split_sublist(inputs, n)

    temp = []
    [temp.extend(_) for _ in outputs];

    assert sorted(temp) == sorted(inputs)
# end def


def test_add_dicts():
    dict_p = {'foo': 'bar'}
    dict_q = {'spam': 'egg', 'ham': 'foobar'}

    ans = opends.shorthand.add_dicts(dict_p, dict_q)
    assert ans == {'foo': 'bar', 'spam': 'egg', 'ham': 'foobar'}
# end def


def test_get_ranking():
    # generate a sequence with known ranking
    t = list(range(10))
    random.shuffle(t)
    s = [None for _ in t]
    val = 0
    for i in range(10):
        j = t.index(i)
        s[j] = val
        val += random.random()
    # end for

    # ascendint = True
    ans = list(opends.shorthand.get_ranking(s, ascending=True))
    assert list(ans) == list(t)
    ans = list(opends.shorthand.get_ranking(s, ascending=False))
    assert list(ans) == list(reversed(t))
# end def





