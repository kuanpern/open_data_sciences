"""
LGLP LICENSE
Copyright (c) 2016 Tan Kuan Pern
"""

import random
import numpy
import pandas
import opends.shorthand
import opends.ML_shorthand


# ========= MACHINE LEARNING ========= #
# construct crossvalidation dataset while maintaining the composition of True and False
def test_construct_CV_datasets():
    # create random data set
    df = pandas.DataFrame(numpy.random.randn(500, 3), )

    for r in [0.333, 0.5, 0.667]:
        # generate random dataset
        while True:
            df['det'] = [random.random() < r for _ in range(len(df))]
            temp = opends.shorthand.count_elements(df['det'])
            r_test = temp[True] / len(df)
            if abs(r_test - r) < 0.1:
                break
            # end if
        # end while
        r = r_test

        # apply the method
        x = opends.ML_shorthand.construct_CV_datasets(df, label_name='det', n_cv=10)

        # ensure ratio is correct
        rs = []
        for fold in x:
            temp = opends.shorthand.count_elements(fold['training']['det'])
            r_test = temp[True] / (temp[True] + temp[False])
            rs.append(r_test)
        # end for
        assert max(numpy.abs(numpy.array(rs) - r_test)) < 0.15

        for fold in x:
            # no overlap between training and testing
            assert len(set(list(fold['training'].index)) & set(list(fold['testing'].index))) == 0

            # no missing data
            len(fold['training']) + len(fold['testing']) == len(df)

            # can get back the original data set
            df_test = fold['training'].append(fold['testing'])
            assert df.equals(df_test.sort_index())
        # end for
    # end for    
# end def


def test_sample_balance_datasets():
    # create random data set
    df = pandas.DataFrame(numpy.random.randn(500, 3), )
    q = 0.95
    for r in [0.333, 0.5, 0.667]:
        # generate random dataset
        while True:
            df['det'] = [random.random() < r for _ in range(len(df))]
            temp = opends.shorthand.count_elements(df['det'])
            r_test = temp[True] / len(df)
            if abs(r_test - r) < 0.1:
                break
            # end if
        # end while

        # apply the function
        x = opends.ML_shorthand.sample_balance_datasets(df, label_name='det', q=q)
        
        for fold in x:
            # check same number of True and False
            assert len(fold[fold['det'] == True]) == len(fold[fold['det'] == False])

            # check no duplicates
            w = fold[fold['det'] == False]
            assert len(w) == len(w.drop_duplicates())
        # end for

        # check most data points are sampled
        indexes = [list(_.index) for _ in x]
        sampled = set(list(numpy.array(indexes).reshape([-1,])))
        sampled_r = len(sampled & set(list(df.index))) / len(set(list(df.index)))
        assert sampled_r > q-0.05
    # end for
# end def
