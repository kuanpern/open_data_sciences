"""  LGLP LICENSE
Copyright (c) 2016 Tan Kuan Pern

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy
import scipy
import random
import opends.shorthand
import opends.stats
import opends.maths


def test_add_stdevs():
    # generate random data
    x = numpy.random.randn(100)
    y = 5*x + 3
    y = [_ + (random.random() - 0.5)*20 for _ in y]
    z = 2*x - 6
    z = [_ + (random.random() - 0.5)*20 for _ in z]

    # correlaration
    r = numpy.corrcoef(x, y)[0][1]

    # stdevs
    stdev_1 = numpy.std(x)
    stdev_2 = numpy.std(y)

    # apply the function
    ans = opends.maths.add_stdevs(stdev_1, stdev_2, r)
    t = numpy.std(list(x + y))
    assert abs(ans - t) / (ans+t - 1) < 0.0001
# end def


def test_substract_stdevs():
    # generate random data
    x = numpy.random.randn(100)
    y = 5*x + 3
    y = [_ + (random.random() - 0.5)*20 for _ in y]
    z = 2*x - 6
    z = [_ + (random.random() - 0.5)*20 for _ in z]

    # correlaration
    r = numpy.corrcoef(x, y)[0][1]

    # stdevs
    stdev_1 = numpy.std(x)
    stdev_2 = numpy.std(y)

    # apply the function
    ans = opends.maths.substract_stdevs(stdev_1, stdev_2, r)
    t = numpy.std(list(x - y))
    assert abs(ans - t) / (ans+t - 1) < 0.0001
# end def


def test_cov2corr():
    # generate random data
    x = numpy.random.randn(100)
    y = 5*x + 3
    y = [_ + (random.random() - 0.5)*20 for _ in y]
    z = 2*x - 6
    z = [_ + (random.random() - 0.5)*20 for _ in z]

    a = numpy.cov([x, y, z])
    b = numpy.corrcoef([x, y, z])

    ans = opends.maths.cov2corr(a)
    assert numpy.abs(ans - b).sum() < 0.0001
# end def


def test_gaussian_coef():
    import scipy.stats

    a = opends.maths.gaussian_coef(101)
    # test symmetry
    assert numpy.all(a == list(reversed(a)))

    # test shape (Gaussian-like)
    x = numpy.arange(-50, 51)
    p = scipy.stats.norm(0, 5)
    y = p.pdf(x)

    assert max(abs(y - a)) < 0.001
# end def


def test_exponential_coef():
    # test sum (geometrical sum formula)
    for _iter in range(20):
        r = random.random()*0.99
        seq = opends.maths.exponential_coef(1000, r)

        # test monotonicity
        seq == list(reversed(sorted(seq)))

        s = sum(seq)
        assert abs(s - 1./(1-r)) < 0.001
    # end for
# end def


def test_eig_decompose():
    # construct matrix
    dim = random.randint(3, 8)
    a = numpy.random.randn(dim, dim)
    b = numpy.linalg.inv(a)
    e = numpy.zeros_like(a)
    for i in range(len(e)):
        e[i][i] = round(random.random(), 2)
    # end for
    A = numpy.asmatrix(a) * numpy.asmatrix(e) * numpy.asmatrix(b)

    # call the function
    p, q, r = opends.maths.eig_decompose(A)

    # test reconstructible
    assert numpy.max(abs(A - p*q*r)) < 0.0001

    # test mutual inverse
    assert numpy.max(abs(p - numpy.linalg.inv(r))) < 0.0001

    # test diagnal matrix
    assert sum(abs(q.diagonal())) - abs(q).sum() < 0.0001
# end def


# discretize a numerical scale into nominal scale. each nominal scale contains approximately equal number of elements.
def test_equal_size_binning():
    import random
    # generate input data
    input_data = range(random.randint(20, 100))
    x = [random.random() for _ in input_data]

    # call the function
    ans = opends.maths.equal_size_binning(x)

    vals = opends.shorthand.count_elements(ans).values()

    # test roughly equal number
    assert (max(vals) - min(vals)) < len(input_data)*0.05

    # test sorting correctly
    import collections
    y = collections.defaultdict(lambda: [])

    for i in range(len(ans)):
        y[ans[i]].append(x[i])
    # end for
    y = dict(y)

    keys = list(sorted(y.keys()))
    for i in range(1, len(keys)-1):
        assert max(y[keys[i]]) <= min(y[keys[i+1]])
    # end for
# end def


def test_filter_outliers():
    import copy

    for _iter in range(20):
        # generate data
        u = random.random()
        s = random.random()

        seq = []
        for i in range(1000):
            val = numpy.random.normal(u, s)
            if abs((val - u) / s) > 2:
                continue
            seq.append(val)
        # end for
        u = numpy.mean(seq)
        s = numpy.std(seq)

        # add outliers
        outliers = []
        for i in range(10):
            val = u + random.randrange(5, 10)*s
            outliers.append(val)
        for i in range(10):
            val = u - random.randrange(5, 10)*s
            outliers.append(val)

        seq.extend(outliers)

        # call the function
        ins, outs, rs = opends.maths.filter_outliers(seq, 2)

        # capture most outliers
        assert len(set(outliers) - set(outs)) < 0.1*len(outliers)

        # retain most inliers
        assert len(ins) >= len(seq)*0.90
    # end for
# end def

