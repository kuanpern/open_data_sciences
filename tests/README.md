### Unit tests for open data sciences library

#### Exclusions
The following 3 functions in "common_utils" are DB connections are not covered in unit tests
 - connect_cassandra_server
 - connect_mongoDB_server
 - connect_hdfs_server


#### Install required packages
```
$ sudo apt install git
$ sudo apt install virtualenv
$ sudo apt install python3-dev
```

#### End-to-end unit test guide 
(1) clone the repository (replace user_name to appropriate value)
```
$ git clone https://{user_name}@bitbucket.org/ucare-tech/ds_common_utils.git
```

(2) set path
```
$ export library_home=/home/ubuntu/ds_common_utils/open_data_sciences
$ export PYTHONPATH=$PYTHONPATH:$library_home
```

#### Prepare virtual environment
```
$ cd $library_home/tests
$ virtualenv -ppython3 venv
$ venv/bin/pip install -r requirements.txt
```

#### Run unit tests
```
$ venv/bin/py.test --cov=. > logs/pytest.log
```

see logs/ for sample log files
