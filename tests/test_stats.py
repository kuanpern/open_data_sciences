"""
LGLP LICENSE
Copyright (c) 2016 Tan Kuan Pern
"""

import numpy
import random
import opends.stats
import opends.shorthand

# generate Fibonacci series
def fib(n, a=0, b=1):
    s = [a, b]
    for i in range(n):
        val = s[-1] + s[-2]
        s.append(val)
    # end for
    return s
# end def

def test_simple_stats():
    t = {'median': 21.0, 'min': 0, 'max': 987, 'N': 17, 'sum': 2583, 
        'stdev': 263.88221, 'mean': 151.94118, 'IQR': 141.0}
    ans = opends.stats.simple_stats(fib(15))

    assert max([abs(ans[key] - t[key]) for key in t.keys()]) < 0.001
# end def


def test_extract_continuous_segments():
    # come out with a (answer) list
    l = []
    choices = ['a', 'b', 'c']
    cur_char = random.choice
    s_index = 0
    for segment in range(random.randrange(0, 15)):
        while True:
            new_char = random.choice(choices)
            if new_char != cur_char:
                cur_char = new_char
                break
            # end if
        # end while
        length = random.randrange(0, 4)
        e_index = s_index + length
        l.append([(s_index, e_index), cur_char])

        s_index = e_index + 1
    # end for

    # generate sequence by the answer
    seq = [None for _ in range(e_index+1)]
    for (s_index, e_index), char in l:
        seq[s_index:e_index+1] = [char]*(e_index-s_index+1)
    # end for

    # test that function returns the answer
    assert opends.stats.extract_continuous_segments(seq) == l
# end def


def test_quantile():
    t = [4, 34, 305]
    ans = opends.stats.quantile(fib(17)) 
    assert list(ans) == list(t)
# end def


def test_IQR():
    ans = opends.stats.IQR(fib(17))
    assert ans == 301
# end def


def test_select_range():
    x = numpy.random.randn(5000)

    ans = opends.stats.select_range(x, sigma=2)

    lb = numpy.mean(x) - 2*numpy.std(x)
    ub = numpy.mean(x) + 2*numpy.std(x)

    err1 = abs(ans[0] - lb) / (abs(lb+ans[0])/2.)
    err2 = abs(ans[1] - ub) / (abs(ub+ans[1])/2.)

    assert max(err1, err2) < 0.05
# end def


def test_weighted_avg_and_std():
    # generate random data
    x = [round(random.random(), 2) for _ in range(20)]
    w = [random.randrange(1, 20) for _ in range(20)]

    # call the function
    ans = opends.stats.weighted_avg_and_std(values=x, weights=w)

    # weights the data manually and apply mean and stdev
    temp = [[x[i]] * w[i] for i in range(len(x))]

    swap = []
    [swap.extend(_) for _ in temp];
    temp = swap

    u, s = numpy.mean(temp), numpy.std(temp)

    # check equal
    assert abs(ans[0] - u) < 0.0001
    assert abs(ans[1] - s) < 0.0001
# end def


def test_discretize_distribution():
    import random
    # generate input data
    input_data = range(random.randint(20, 100))
    x = [random.random() for _ in input_data]

    # call the function
    ans = opends.stats.discretize_distribution(x)

    vals = opends.shorthand.count_elements(ans).values()

    # test roughly equal number
    assert (max(vals) - min(vals)) < len(input_data)*0.05

    # test sorting correctly
    import collections
    y = collections.defaultdict(lambda: [])

    for i in range(len(ans)):
        y[ans[i]].append(x[i])
    # end for
    y = dict(y)

    keys = list(sorted(y.keys()))
    for i in range(1, len(keys)-1):
        assert max(y[keys[i]]) <= min(y[keys[i+1]])
    # end for
# end def


def test_RouletteWheel():
    # generate random fitness
    labels = list('abcdefg')
    vals = [random.random() for _ in labels]
    fitness = dict(zip(labels, vals))

    # make the wheel
    wheel = opends.stats.RouletteWheel(fitness)

    # sample the wheel
    samples = [wheel.get() for _ in range(100000)]

    # get the distribution from the samples
    p = opends.shorthand.normalize_dict(opends.shorthand.count_elements(samples))

    # ensure the distribution and samples are similar
    q = opends.shorthand.normalize_dict(fitness)
    d = numpy.mean([abs(p[key] - q[key]) for key in q.keys()])
    assert d < 0.05
# end class


# cumulative plot
def cum_plot():
    # generate random data
    import random
    data = [random.random() for _ in range(1000)]

    # call the function
    xs, ys = opends.stats.cum_plot(data, xs_in, matplot=False)
    xs_in = numpy.arange(0, 101, 2)

    # input check
    assert numpy.all(xs == xs_in)

    # check monotonicity
    assert list(sorted(ys)) == list(ys)

    # ensure sum to 1
    assert abs(1 - ys[-1]) < 0.001
# end def


def test_compute_confusion_matrix():
    # generate random data
    import random
    t = {
        'TP': random.randrange(10, 100),
        'FP': random.randrange(10, 100),
        'TN': random.randrange(10, 100),
        'FN': random.randrange(10, 100)
    }
    seq = t['FN']*[(False, True)]  + \
          t['TP']*[(True , True)]  + \
          t['FP']*[(True , False)] + \
          t['TN']*[(False, False)]
    random.shuffle(seq)
    preds, labels = list(zip(*seq))

    # call the function
    ans = opends.stats.compute_confusion_matrix(preds, labels)

    # verify get back correct answer
    assert ans == t
# end def


def test_binary_classification_assessment():
    # test case 1 (normal)
    c_in = {'FN': 12, 'FP': 20, 'TN': 36, 'TP': 52}
    t = {
      'TP': 52, 'FN': 12, 'FP': 20, 'TN': 36, 
      'ACC': 0.7333, 'MCC': 0.0001, 'F1': 0.7647, 
      'TPR': 0.8125, 'TNR': 0.6429, 
      'FNR': 0.1875, 'FDR': 0.2778, 
      'FPR': 0.3571, 'PPV': 0.7222, 
      'NPV': 0.7500, 'FOR': 0.2500, 
       'MK': 0.4722,  'BM': 0.4554, 
    } 
    ans = opends.stats.binary_classification_assessment(c_in)
    assert sum(abs(ans[key] - t[key]) for key in t.keys()) < 0.001

    # test case 2 (all right)
    c_in = {'TP': 100, 'FP': 0, 'FN':0, 'TN':100}
    t = {'ACC': 1.0, 'BM': 1.0, 'F1': 1.0, 'FDR': 0.0, 
         'FN': 0.0,  'FNR': 0.0, 'FOR': 0.0, 'FP': 0.0, 
         'FPR': 0.0, 'MCC': 0.0, 'MK': 1.0, 'NPV': 1.0, 
         'PPV': 1.0, 'TN': 100, 'TNR': 1.0, 'TP': 100, 
         'TPR': 1.0}
    ans = opends.stats.binary_classification_assessment(c_in)
    assert sum(abs(ans[key] - t[key]) for key in t.keys()) < 0.001

    # test case 3 (all wrong)
    c_in = {'TP': 0, 'FP': 100, 'FN':100, 'TN':0}
    t = {'ACC': 0.0, 'BM': -1.0, 'F1': 0.0, 'FDR': 1.0, 
         'FN': 100, 'FNR': 1.0, 'FOR': 1.0, 'FP': 100, 
         'FPR': 1.0, 'MCC': 0, 'MK': -1.0, 'NPV': 0.0, 
         'PPV': 0.0, 'TN': 0.0, 'TNR': 0.0, 'TP': 0.0, 
         'TPR': 0.0}
    ans = opends.stats.binary_classification_assessment(c_in)
    assert sum(abs(ans[key] - t[key]) for key in t.keys()) < 0.001
# end def


# function to transfer data from one histogram to another with different bins
def test_histogram_change_base():
    import scipy.integrate

    x = numpy.random.randn(5000,)
    ys, xs = numpy.histogram(x, 15)
    mids = (xs[1:] + xs[:-1]) / 2.
    H = (mids, ys)

    # compute area
    area_p = scipy.integrate.simps(x=mids, y=ys)

    # switch 0.5 step
    dx = numpy.mean(numpy.diff(mids)) * 0.5
    xs_new = mids + dx

    # call the function
    xs_new, ys_new = opends.stats.histogram_change_base(H, xs_new)

    # compute new area
    area_q = scipy.integrate.simps(x=xs_new, y=ys_new)

    # ensure area difference less than 10%
    area_r = area_p *len(ys_new)/len(ys)
    assert abs(area_q - area_r) / ((area_q+area_r)/2) < 0.1
# end def
