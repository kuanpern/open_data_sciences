import opends.common_utils


def test_gen_mongodb_url():
    ### basic input
    pars1 = {
        'username': 'foo',
        'password': 'bar',
        'cluster_url': '127.0.0.1',
        'database': 'test_db',
    }
    ans = opends.common_utils.gen_mongodb_url(pars1)
    assert ans == 'mongodb://foo:bar@127.0.0.1/test_db'
    
    ### advanced input
    pars2 = {
        'authSource': 'users',
        'replicaSetName': 'testReplicaSet', 
        'readPreference': 'secondary',
    }
    pars2.update(pars1)
    
    ans = opends.common_utils.gen_mongodb_url(pars2)
    assert ans == 'mongodb://foo:bar@127.0.0.1/test_db?authSource=users&replicaSet=testReplicaSet&readPreference=secondary'
    
    ### test input validity
    pars3 = {}
    ans = opends.common_utils.gen_mongodb_url({})
    assert ans['status'] == 'failed'
    
# end def
